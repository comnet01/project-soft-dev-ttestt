/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.MonthReport;
import com.nawapat.dcoffeeproject.model.SalesReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parametsmac
 */
public class MonthReportDao implements Dao<MonthReport> {

    @Override
    public MonthReport get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<MonthReport> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public MonthReport save(MonthReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public MonthReport update(MonthReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(MonthReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<MonthReport> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

//    public List<MonthReport> getMonthReport(int limit) {
//        ArrayList<MonthReport> rlist = new ArrayList();
//        String sql = """
//                     SELECT material_id, material_name, material_remain , material_unit
//                     FROM material 
//                     ORDER BY material_remain ASC 
//                     LIMIT ?;""";
//        
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, limit);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                MonthReport obj = MonthReport.fromRS(rs);
//                rlist.add(obj);
//
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return rlist;
//    }
    public List<MonthReport> getMonthReport() {
        ArrayList<MonthReport> rlist = new ArrayList();
        String sql = """
                 SELECT
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-01-01' AND '2023-01-31') as subquery1)
                 as total_sales_januray, 
                     
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-02-01' AND '2023-02-31') as subquery1)
                 as total_sales_feb,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-03-01' AND '2023-03-31') as subquery1)
                 as total_sales_march,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-04-01' AND '2023-04-31') as subquery1)
                 as total_sales_april,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-05-01' AND '2023-05-31') as subquery1)
                 as total_sales_may,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-06-01' AND '2023-06-31') as subquery1)
                 as total_sales_june,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-07-01' AND '2023-07-31') as subquery1)
                 as total_sales_july,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-08-01' AND '2023-08-31') as subquery1)
                 as total_sales_august,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-09-01' AND '2023-09-31') as subquery1)
                 as total_sales_september,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-10-01' AND '2023-10-31') as subquery1)
                 as total_sales_october,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-11-01' AND '2023-11-30') as subquery2)
                 as total_sales_november,
                 
                 (SELECT SUM(net_total)
                 FROM
                 (SELECT r.reciept_id,
                 r.reciept_date,
                 (r.total - COALESCE(e.total, 0) - COALESCE(m.material_invoice_total, 0)) as net_total
                 FROM reciept r
                 LEFT JOIN
                 (SELECT expanse_id, SUM(total) as total
                 FROM expanse
                 GROUP BY expanse_id) e
                 ON r.reciept_id = e.expanse_id
                 LEFT JOIN
                 (SELECT material_invoice_id, SUM(material_invoice_total) as material_invoice_total
                 FROM material_invoice
                 GROUP BY material_invoice_id) m
                 ON r.reciept_id = m.material_invoice_id
                 WHERE r.reciept_date BETWEEN '2023-12-01' AND '2023-12-30') as subquery2)
                 as total_sales_december;
                 """;

        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                MonthReport obj = MonthReport.fromRS(rs);
                rlist.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return rlist;
    }

}

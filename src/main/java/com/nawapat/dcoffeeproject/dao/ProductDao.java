/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.ProductModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gxz32
 */
public class ProductDao implements Dao<ProductModel> {

    @Override
    public ProductModel get(int id) {
        ProductModel product = null;
        String sql = "SELECT * FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                product = ProductModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }

    public List<ProductModel> getAll() {
        ArrayList<ProductModel> list = new ArrayList();
        String sql = "SELECT * FROM product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductModel product = ProductModel.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ProductModel> getAll(String where, String order) {
        ArrayList<ProductModel> list = new ArrayList();
        String sql = "SELECT * FROM product where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductModel product = ProductModel.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ProductModel> getAllBywhere(String where) {
        ArrayList<ProductModel> list = new ArrayList();
        String sql = "SELECT * FROM product where " + where;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductModel product = ProductModel.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ProductModel> getAllBySearch1(String search) {
        ArrayList<ProductModel> list = new ArrayList();
        String sql = "SELECT * FROM product WHERE product_category = 1 And product_name LIKE ? || '%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, search);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductModel product = ProductModel.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ProductModel> getAllBySearch2(String search) {
        ArrayList<ProductModel> list = new ArrayList();
        String sql = "SELECT * FROM product WHERE product_category = 2 And product_name LIKE ? || '%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, search);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductModel product = ProductModel.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ProductModel> getAll(String order) {
        ArrayList<ProductModel> list = new ArrayList();
        String sql = "SELECT * FROM product  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductModel product = ProductModel.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ProductModel save(ProductModel obj) {

        String sql = "INSERT INTO product (product_key, product_name, product_price, product_size, product_sweet_level, product_category)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getKey());
            stmt.setString(2, obj.getName());
            stmt.setFloat(3, obj.getPrice());
            stmt.setString(4, obj.getSize());
            stmt.setString(5, obj.getSweetLevel());
            stmt.setInt(6, obj.getCategory());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ProductModel update(ProductModel obj) {
        String sql = "UPDATE product"
                + " SET product_key = ?, product_name = ?, product_price = ?, product_size = ?, product_sweet_level = ?, product_category = ?"
                + " WHERE product_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getKey());
            stmt.setString(2, obj.getName());
            stmt.setFloat(3, obj.getPrice());
            stmt.setString(4, obj.getSize());
            stmt.setString(5, obj.getSweetLevel());
            stmt.setInt(6, obj.getCategory());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ProductModel obj) {
        String sql = "DELETE FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}

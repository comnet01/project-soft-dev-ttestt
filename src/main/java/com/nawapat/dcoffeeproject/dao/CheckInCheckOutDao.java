/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.CheckInCheckOutModel;
import com.nawapat.dcoffeeproject.model.PaymentModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class CheckInCheckOutDao implements Dao<CheckInCheckOutModel> {

    private CheckInCheckOutModel obj;

    @Override
    public CheckInCheckOutModel get(int id) {
        CheckInCheckOutModel checkincheckout = null;
        String sql = "SELECT * FROM checkin_checkout WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkincheckout = CheckInCheckOutModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkincheckout;
    }

    public CheckInCheckOutModel getStatus(int id) {
        CheckInCheckOutModel checkincheckout = null;
        String sql = "SELECT * FROM checkin_checkout WHERE status=0";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkincheckout = CheckInCheckOutModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkincheckout;
    }

    public List<CheckInCheckOutModel> getAll() {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = "SELECT * FROM checkin_checkout";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInCheckOutModel checkincheckout = CheckInCheckOutModel.fromRS(rs);
                list.add(checkincheckout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

//    @Override
//    public List<CheckInCheckOutModel> getAll(String where, String order) {
//        ArrayList<CheckInCheckOutModel> list = new ArrayList();
//        String sql = "SELECT * FROM checkin_checkout where " + where + " ORDER BY" + order;
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery(sql);
//
//            while (rs.next()) {
//                CheckInCheckOutModel checkincheckout = CheckInCheckOutModel.fromRS(rs);
//                list.add(checkincheckout);
//
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return list;
//    }
    public List<CheckInCheckOutModel> getAll(String order) {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = "SELECT * FROM checkin_checkout  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInCheckOutModel checkincheckout = CheckInCheckOutModel.fromRS(rs);
                list.add(checkincheckout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInCheckOutModel> getAllEmp(String order) {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = """
                     SELECT *,us.user_id,em.employee_id,cc.user_id,cc.id,em.employee_key,sum(duration),em.employee_hourysalary,sum(duration)*em.employee_hourysalary,cc.status FROM checkin_checkout cc INNER JOIN user us ON cc.user_id = us.user_id INNER JOIN employee em ON em.employee_id = us.employee_id
                     GROUP BY us.user_id 
                     ORDER BY 
                     
                     """ + order;

        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInCheckOutModel checkincheckout = CheckInCheckOutModel.fromRS(rs);
                list.add(checkincheckout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
//    @Override
//    public List<CheckInCheckOutModel> getAll(String begin , String ) {
//        ArrayList<CheckInCheckOutModel> list = new ArrayList();
//        String sql = "SELECT * FROM checkin_checkout";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery(sql);
//
//            while (rs.next()) {
//                CheckInCheckOutModel checkincheckout = CheckInCheckOutModel.fromRS(rs);
//                list.add(checkincheckout);
//
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return list;
//    }

    public List<CheckInCheckOutModel> getAll(String begin, String end) {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = """
                      SELECT * From checkin_checkout cc 
                      INNER JOIN user u ON u.user_id = cc.user_id
                      INNER JOIN employee em ON em.employee_id = u.employee_id AND cc.checkin BETWEEN ? AND ?
                      GROUP BY cc.id
                      ORDER BY cc.id DESC
                      ;     
                      """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckInCheckOutModel obj = CheckInCheckOutModel.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return list;

    }

    public List<CheckInCheckOutModel> getAllBySearch(String searchId, String searchName, String begin, String end) {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = "SELECT * FROM checkin_checkout cc INNER JOIN user us ON cc.user_id = us.user_id  INNER JOIN employee em ON em.employee_id = us.user_id WHERE employee_key Like ? || '%' AND employee_name Like ? || '%' AND checkin BETWEEN ? AND ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, searchId);
            stmt.setString(2, searchName);
            stmt.setString(3, begin);
            stmt.setString(4, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckInCheckOutModel inout = CheckInCheckOutModel.fromRS(rs);
                list.add(inout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInCheckOutModel save(CheckInCheckOutModel obj) {

        String sql = "INSERT INTO checkin_checkout (duration, status, payment_id , user_id)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getDuration());
            stmt.setInt(2, obj.getStatus());
            stmt.setInt(3, obj.getPaymentid());
            stmt.setInt(4, obj.getUserid());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckInCheckOutModel update(CheckInCheckOutModel obj) {
        String sql = "UPDATE checkin_checkout"
                + " SET duration = ?, status = ?, payment_id = ?, user_id = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getDuration());
            stmt.setInt(2, obj.getStatus());
            stmt.setInt(3, obj.getPaymentid());
            stmt.setInt(4, obj.getUserid());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public CheckInCheckOutModel updateCheckoutTime(int id) {
        String sql = "UPDATE checkin_checkout "
                + "SET checkout = datetime('now', 'localtime'), "
                + "duration = strftime('%H', datetime('now', 'localtime')) - strftime('%H', checkin) "
                + "+ (strftime('%M', datetime('now', 'localtime')) - strftime('%M', checkin)) / 60 "
                + "WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckInCheckOutModel obj) {
        String sql = "DELETE FROM checkin_checkout WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<CheckInCheckOutModel> getAllByKey(String key) {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = """
                     SELECT *
                     FROM checkin_checkout cc
                     INNER JOIN user us ON cc.user_id = us.user_id
                     INNER JOIN employee em ON us.employee_id = em.employee_id WHERE em.employee_key = ?;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, key);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckInCheckOutModel inout = CheckInCheckOutModel.fromRS(rs);
                list.add(inout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInCheckOutModel> getAllById(String key) {
        ArrayList<CheckInCheckOutModel> list = new ArrayList();
        String sql = """
                     SELECT *
                                          FROM checkin_checkout cc
                                          INNER JOIN user us ON cc.user_id = us.user_id
                                          INNER JOIN employee em ON us.employee_id = em.employee_id WHERE em.employee_key = ? AND status = 0;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, key);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckInCheckOutModel inout = CheckInCheckOutModel.fromRS(rs);
                list.add(inout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}

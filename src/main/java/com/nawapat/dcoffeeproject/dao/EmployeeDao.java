/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.CheckInCheckOutModel;
import com.nawapat.dcoffeeproject.model.EmployeeModel;
import com.nawapat.dcoffeeproject.model.PaymentModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class EmployeeDao implements Dao<EmployeeModel> {

    @Override
    public EmployeeModel get(int id) {
        EmployeeModel employee = null;
        String sql = "SELECT * FROM employee WHERE employee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = EmployeeModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }

    public EmployeeModel getByPhone(String name) {
        EmployeeModel employee = null;
        String sql = "SELECT * FROM employee WHERE employee_phone=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = EmployeeModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }
    
    public EmployeeModel getByKey(String name) {
        EmployeeModel employee = null;
        String sql = "SELECT * FROM employee WHERE employee_key=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = EmployeeModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }

    public List<EmployeeModel> getAll() {
        ArrayList<EmployeeModel> list = new ArrayList();
        String sql = "SELECT * FROM employee";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                EmployeeModel employee = EmployeeModel.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<EmployeeModel> getAll(String where, String order) {
        ArrayList<EmployeeModel> list = new ArrayList();
        String sql = "SELECT * FROM employee where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                EmployeeModel employee = EmployeeModel.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<EmployeeModel> getAll(String order) {
        ArrayList<EmployeeModel> list = new ArrayList();
        String sql = "SELECT * FROM employee  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                EmployeeModel employee = EmployeeModel.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public EmployeeModel save(EmployeeModel obj) {

        String sql = "INSERT INTO employee (employee_key, employee_name, employee_phone, employee_position , employee_hourysalary)"
                + "VALUES(?, ?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getKey());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getPhone());
            stmt.setInt(4, obj.getPosition());
            stmt.setFloat(5, obj.getHoursalary());
            
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public EmployeeModel update(EmployeeModel obj) {
        String sql = "UPDATE employee"
                + " SET employee_key = ?, employee_name = ?, employee_phone = ?, employee_position = ? , employee_hourysalary = ?"
                + " WHERE employee_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getKey());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getPhone());
            stmt.setInt(4, obj.getPosition());
            stmt.setFloat(5, obj.getHoursalary());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(EmployeeModel obj) {
        String sql = "DELETE FROM employee WHERE employee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public ArrayList<EmployeeModel> getBySearch(String key, String name) {
        ArrayList<EmployeeModel> list = new ArrayList();
        String sql = """
                     SELECT * FROM employee 
                     WHERE employee_key Like ? || '%' AND employee_name Like ? || '%' 
                     
                     """ ;

        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, key);
            stmt.setString(2, name);
         
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                EmployeeModel emp = EmployeeModel.fromRS(rs);
                list.add(emp);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.HistoryMatDetailModel;
import com.nawapat.dcoffeeproject.model.HistoryMatModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class HistoryMatDao implements Dao<HistoryMatModel> {

    @Override
    public HistoryMatModel get(int id) {
        HistoryMatModel hisMaterial = null;
        String sql = "SELECT * FROM history_material WHERE history_material_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                hisMaterial = HistoryMatModel.fromRS(rs);
                HistoryMatDetailDao rdd = new HistoryMatDetailDao();
                ArrayList<HistoryMatDetailModel> matDetails = (ArrayList<HistoryMatDetailModel>) rdd.getAll("history_material_id=" + hisMaterial.getId(), " history_material_detail_id ");
                hisMaterial.setHisMatDetail(matDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return hisMaterial;
    }

    public List<HistoryMatModel> getAll() {
        ArrayList<HistoryMatModel> list = new ArrayList();
        String sql = "SELECT * FROM history_material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryMatModel hisMaterial = HistoryMatModel.fromRS(rs);
                list.add(hisMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<HistoryMatModel> getAll(String where, String order) {
        ArrayList<HistoryMatModel> list = new ArrayList();
        String sql = "SELECT * FROM history_material where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryMatModel hisMaterial = HistoryMatModel.fromRS(rs);
                list.add(hisMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<HistoryMatModel> getAll(String order) {
        ArrayList<HistoryMatModel> list = new ArrayList();
        String sql = "SELECT * FROM history_material  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryMatModel hisMaterial = HistoryMatModel.fromRS(rs);
                list.add(hisMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<HistoryMatModel> getAllbyDate(String begin, String end, String order) {
        ArrayList<HistoryMatModel> list = new ArrayList();
        String sql = "SELECT * FROM history_material WHERE history_material_date BETWEEN ? AND ? ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, begin);
            pstmt.setString(2, end);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                HistoryMatModel hisMaterial = HistoryMatModel.fromRS(rs);
                list.add(hisMaterial);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public HistoryMatModel save(HistoryMatModel obj) {

        String sql = "INSERT INTO history_material (user_id)"
                + "VALUES(?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public HistoryMatModel update(HistoryMatModel obj) {
        String sql = "UPDATE history_material"
                + " SET user_id = ?"
                + " WHERE history_material_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());
            stmt.setInt(2, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(HistoryMatModel obj) {
        String sql = "DELETE FROM history_material WHERE history_material_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}

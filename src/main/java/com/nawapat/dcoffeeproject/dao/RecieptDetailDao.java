/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.RecieptDetailModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptDetailDao implements Dao<RecieptDetailModel> {

    @Override
    public RecieptDetailModel get(int id) {
        RecieptDetailModel recieptDetail = null;
        String sql = "SELECT * FROM reciept_detail WHERE reciept_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                recieptDetail = RecieptDetailModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return recieptDetail;
    }

    public List<RecieptDetailModel> getAll() {
        ArrayList<RecieptDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM reciept_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetailModel recieptDetail = RecieptDetailModel.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<RecieptDetailModel> getAll(String where, String order) {
        ArrayList<RecieptDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM reciept_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetailModel recieptDetail = RecieptDetailModel.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<RecieptDetailModel> getAll(String order) {
        ArrayList<RecieptDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM reciept_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetailModel recieptDetail = RecieptDetailModel.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<RecieptDetailModel> getAllById(int id) {
        ArrayList<RecieptDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM reciept_detail WHERE reciept_id  =? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RecieptDetailModel recieptDetail = RecieptDetailModel.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public RecieptDetailModel save(RecieptDetailModel obj) {

        String sql = "INSERT INTO reciept_detail (product_id, product_name, product_price, product_size, product_sweet_level, qty, total_price, reciept_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProductId());
            stmt.setString(2, obj.getProductName());
            stmt.setFloat(3, obj.getProductPrice());
            stmt.setString(4, obj.getProductSize());
            stmt.setString(5, obj.getProductSweetLevel());
            stmt.setInt(6, obj.getQty());
            stmt.setFloat(7, obj.getTotalPrice());
            stmt.setInt(8, obj.getRecieptId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public RecieptDetailModel update(RecieptDetailModel obj) {
        String sql = "UPDATE reciept_detail"
                + " SET product_id = ?, product_name = ?, product_price = ?, qty = ?, total_price = ?, reciept_id = ?"
                + " WHERE reciept_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProductId());
            stmt.setString(2, obj.getProductName());
            stmt.setFloat(3, obj.getProductPrice());
            stmt.setInt(4, obj.getQty());
            stmt.setFloat(5, obj.getTotalPrice());
            stmt.setInt(6, obj.getRecieptId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(RecieptDetailModel obj) {
        String sql = "DELETE FROM reciept_detail WHERE reciept_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.MaterialModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class MaterialDao implements Dao<MaterialModel> {

    @Override
    public MaterialModel get(int id) {
        MaterialModel material = null;
        String sql = "SELECT * FROM material WHERE material_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                material = MaterialModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return material;
    }

    public MaterialModel getByKey(String name) {
        MaterialModel material = null;
        String sql = "SELECT * FROM material WHERE material_key=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                material = MaterialModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return material;
    }

    public List<MaterialModel> getAll() {
        ArrayList<MaterialModel> list = new ArrayList();
        String sql = "SELECT * FROM material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialModel material = MaterialModel.fromRS(rs);
                list.add(material);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<MaterialModel> getAll(String where, String order) {
        ArrayList<MaterialModel> list = new ArrayList();
        String sql = "SELECT * FROM material where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialModel material = MaterialModel.fromRS(rs);
                list.add(material);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<MaterialModel> getAll(String order) {
        ArrayList<MaterialModel> list = new ArrayList();
        String sql = "SELECT * FROM material  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialModel material = MaterialModel.fromRS(rs);
                list.add(material);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public MaterialModel save(MaterialModel obj) {

        String sql = "INSERT INTO material (material_key, material_name, material_price, material_unit, material_minimum, material_remain, material_req)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getKey());
            stmt.setString(2, obj.getName());
            stmt.setFloat(3, obj.getPrice());
            stmt.setString(4, obj.getUnit());
            stmt.setInt(5, obj.getMin());
            stmt.setInt(6, obj.getRemain());
            stmt.setInt(7, obj.getRequestBuy());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public MaterialModel update(MaterialModel obj) {
        String sql = "UPDATE material"
                + " SET material_key = ?, material_name = ?, material_price = ?, material_unit = ?, material_minimum = ?, Material_remain = ?, material_req = ?"
                + " WHERE material_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getKey());
            stmt.setString(2, obj.getName());
            stmt.setFloat(3, obj.getPrice());
            stmt.setString(4, obj.getUnit());
            stmt.setInt(5, obj.getMin());
            stmt.setInt(6, obj.getRemain());
            stmt.setInt(7, obj.getRequestBuy());
            stmt.setInt(8, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(MaterialModel obj) {
        String sql = "DELETE FROM material WHERE material_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public MaterialModel clearMat(MaterialModel obj) {

        String sql = "UPDATE material SET material_req = 0 WHERE material_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getRequestBuy());
            stmt.setInt(2, obj.getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.PaymentModel;
import com.nawapat.dcoffeeproject.model.ProductModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class PaymentDao implements Dao<PaymentModel> {

    @Override
    public PaymentModel get(int id) {
        PaymentModel payment = null;
        String sql = "SELECT * FROM payment WHERE payment_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                payment = PaymentModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return payment;
    }

    public List<PaymentModel> getAll() {
        ArrayList<PaymentModel> list = new ArrayList();
        String sql = "SELECT * FROM payment";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PaymentModel payment = PaymentModel.fromRS(rs);
                list.add(payment);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<PaymentModel> getAll(String where, String order) {
        ArrayList<PaymentModel> list = new ArrayList();
        String sql = "SELECT * FROM payment where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PaymentModel payment = PaymentModel.fromRS(rs);
                list.add(payment);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<PaymentModel> getAll(String order) {
        ArrayList<PaymentModel> list = new ArrayList();
        String sql = "SELECT * FROM payment  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PaymentModel payment = PaymentModel.fromRS(rs);
                list.add(payment);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<PaymentModel> getAllBySearchName(String search) {
        ArrayList<PaymentModel> list = new ArrayList();
        String sql = "SELECT * FROM payment pm INNER JOIN employee em ON pm.employee_id = em.employee_id WHERE employee_name Like ? || '%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, search);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PaymentModel payment = PaymentModel.fromRS(rs);
                list.add(payment);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<PaymentModel> getAllBySearchId(String search) {
        ArrayList<PaymentModel> list = new ArrayList();
        String sql = "SELECT * FROM payment pm INNER JOIN employee em ON pm.employee_id = em.employee_id WHERE employee_key Like ? || '%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, search);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PaymentModel payment = PaymentModel.fromRS(rs);
                list.add(payment);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<PaymentModel> getAllBySearch(String searchId , String searchName) {
        ArrayList<PaymentModel> list = new ArrayList();
        String sql = "SELECT * FROM payment pm INNER JOIN employee em ON pm.employee_id = em.employee_id WHERE employee_key Like ? || '%' AND employee_name Like ? || '%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, searchId);
            stmt.setString(2, searchName);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PaymentModel payment = PaymentModel.fromRS(rs);
                list.add(payment);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<PaymentModel> getAllBySearch(String searchId , String searchName , String begin , String end) {
        ArrayList<PaymentModel> list = new ArrayList();
        String sql = "SELECT * FROM payment pm INNER JOIN employee em ON pm.employee_id = em.employee_id WHERE employee_key Like ? || '%' AND employee_name Like ? || '%' AND payment_date BETWEEN ? AND ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, searchId);
            stmt.setString(2, searchName);
            stmt.setString(3, begin);
            stmt.setString(4, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PaymentModel payment = PaymentModel.fromRS(rs);
                list.add(payment);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public PaymentModel save(PaymentModel obj) {

        String sql = "INSERT INTO payment (employee_id, payment_total)"
                + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setFloat(2, obj.getTotal());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public PaymentModel update(PaymentModel obj) {
        String sql = "UPDATE payment"
                + " SET employee_id = ?, payment_total = ?"
                + " WHERE payment_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setFloat(2, obj.getTotal());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(PaymentModel obj) {
        String sql = "DELETE FROM payment WHERE payment_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}

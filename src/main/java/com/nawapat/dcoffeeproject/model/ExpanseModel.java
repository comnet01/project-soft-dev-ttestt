/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kornn
 */
public class ExpanseModel {

    private int id;
    private Date bill_date;
    private String bill_name;
    private float ft_unit;
    private int total_unit;
    private float total;
    private Iterable<ExpanseModel> expanse;

    public ExpanseModel(int id, Date bill_date, String bill_name, float ft_unit, int total_unit, float total) {
        this.id = id;
        this.bill_date = bill_date;
        this.bill_name = bill_name;
        this.ft_unit = ft_unit;
        this.total_unit = total_unit;
        this.total = total;
    }

    public ExpanseModel(Date bill_date, String bill_name, float ft_unit, int total_unit, float total) {
        this.id = -1;
        this.bill_date = bill_date;
        this.bill_name = bill_name;
        this.ft_unit = ft_unit;
        this.total_unit = total_unit;
        this.total = total;
    }

    public ExpanseModel() {
        this.id = -1;
        this.bill_date = null;
        this.bill_name = "";
        this.ft_unit = 0;
        this.total_unit = 0;
        this.total = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getBill_date() {
        return bill_date;
    }

    public void setBill_date(Date bill_date) {
        this.bill_date = bill_date;
    }

    public String getBill_name() {
        return bill_name;
    }

    public void setBill_name(String bill_name) {
        this.bill_name = bill_name;
    }

    public float getFt_unit() {
        return ft_unit;
    }

    public void setFt_unit(float ft_unit) {
        this.ft_unit = ft_unit;
    }

    public int getTotal_unit() {
        return total_unit;
    }

    public void setTotal_unit(int total_unit) {
        this.total_unit = total_unit;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ExpanseModel{" + "id=" + id + ", bill_date=" + bill_date + ", bill_name=" + bill_name + ", ft_unit=" + ft_unit + ", total_unit=" + total_unit + ", total=" + total + '}';
    }

    public void calculateTotal() {
        float total = 0.0f;
        for (ExpanseModel exp : expanse) {
            total = exp.getFt_unit() * exp.getTotal_unit();

        }
        this.total = total;
    }

    public static ExpanseModel fromRS(ResultSet rs) {
        ExpanseModel expanse = new ExpanseModel();
        try {
            expanse.setId(rs.getInt("expanse_id"));
            expanse.setBill_date(rs.getDate("bill_date"));
            expanse.setBill_name(rs.getString("bill_name"));
            expanse.setFt_unit(rs.getFloat("ft_unit"));
            expanse.setTotal_unit(rs.getInt("total_unit"));
            expanse.setTotal(rs.getFloat("total"));
        } catch (SQLException ex) {
            Logger.getLogger(ExpanseModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expanse;
    }

}

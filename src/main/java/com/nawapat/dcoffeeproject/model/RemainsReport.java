/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author parametsmac
 */
public class RemainsReport {
    
    private int id;
    private String name;
    private int totalRemain;
    private String unit;
    
    public RemainsReport(int id, String name, int totalRemain, String unit) {
        this.id = id;
        this.name = name;
        this.totalRemain = totalRemain;
        this.unit = unit;
    }
    
    public RemainsReport(String name, int totalRemain, String unit) {
        this.id = -1;
        this.name = name;
        this.totalRemain = totalRemain;
        this.unit = unit;
    }
    
    public RemainsReport() {
        this.id = -1;
        this.name = "";
        this.totalRemain = 0;
        this.unit = "";
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public int getTotalRemain() {
        return totalRemain;
    }

    public void setTotalRemain(int totalRemain) {
        this.totalRemain = totalRemain;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "RemainsReport{" + "id=" + id + ", name=" + name + ", totalRemain=" + totalRemain + ", unit=" + unit + '}';
    }
    
    
    
    
    
    public static RemainsReport fromRS(ResultSet rs) {
        RemainsReport obj = new RemainsReport();
        try {
            obj.setId(rs.getInt("material_id"));
            obj.setName(rs.getString("material_name"));
            obj.setTotalRemain(rs.getInt("material_remain"));
            obj.setUnit(rs.getString("material_unit"));
        } catch (SQLException ex) {
            Logger.getLogger(RemainsReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}

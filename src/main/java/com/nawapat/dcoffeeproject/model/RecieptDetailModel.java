/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gxz32
 */
public class RecieptDetailModel {

    private int Id;
    private int productId;
    private String productName;
    private float productPrice;
    private String productSize;
    private String productSweetLevel;
    private int qty;
    private float totalPrice;
    private int recieptId;

    public RecieptDetailModel(int Id, int productId, String productName, float productPrice, String productSize, String productSweetLevel, int qty, float totalPrice, int recieptId) {
        this.Id = Id;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.productSweetLevel = productSweetLevel;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
    }
    
    public RecieptDetailModel(int productId, String productName, float productPrice, String productSize, String productSweetLevel, int qty, float totalPrice, int recieptId) {
        this.Id = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.productSweetLevel = productSweetLevel;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
    }
    
    public RecieptDetailModel() {
        this.Id = -1;
        this.productId = 0;
        this.productName = "";
        this.productPrice = 0;
        this.productSize = "";
        this.productSweetLevel = "";
        this.qty = 0;
        this.totalPrice = 0;
        this.recieptId = 0;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductSweetLevel() {
        return productSweetLevel;
    }

    public void setProductSweetLevel(String productSweetLevel) {
        this.productSweetLevel = productSweetLevel;
    }
    
    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty * productPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    @Override
    public String toString() {
        return "RecieptDetailModel{" + "Id=" + Id + ", productId=" + productId + ", productName=" + productName + 
                ", productPrice=" + productPrice + ", productSize=" + productSize + ", productSweetLevel="
                + productSweetLevel + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recieptId=" + recieptId + '}';
    }

    public static RecieptDetailModel fromRS(ResultSet rs) {
        RecieptDetailModel recieptDetail = new RecieptDetailModel();
        try {
            recieptDetail.setId(rs.getInt("reciept_detail_id"));
            recieptDetail.setProductId(rs.getInt("product_id"));
            recieptDetail.setProductName(rs.getString("product_name"));
            recieptDetail.setProductPrice(rs.getFloat("product_price"));
            recieptDetail.setProductSize(rs.getString("product_size"));
            recieptDetail.setProductSweetLevel(rs.getString("product_sweet_level"));
            recieptDetail.setQty(rs.getInt("qty"));
            recieptDetail.setTotalPrice(rs.getFloat("total_price"));
            recieptDetail.setRecieptId(rs.getInt("reciept_id"));
        } catch (SQLException ex) {
            Logger.getLogger(CustomerModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }
}

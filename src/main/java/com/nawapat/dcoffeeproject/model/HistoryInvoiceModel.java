/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import com.nawapat.dcoffeeproject.dao.UserDao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class HistoryInvoiceModel {

    private int id;
    private Date date;
    private float total;
    private int userId;
    private UserModel user;
    private ArrayList<HistoryInvoiceDetailModel> hisInvoices = new ArrayList<HistoryInvoiceDetailModel>();

    public HistoryInvoiceModel(int id, Date date, float total, int userId) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.userId = userId;
    }

    public HistoryInvoiceModel(Date date, float total, int userId) {
        this.id = -1;
        this.date = date;
        this.total = total;
        this.userId = userId;
    }

    public HistoryInvoiceModel(int userId) {
        this.id = -1;
        this.date = null;
        this.total = 0;
        this.userId = userId;
    }

    public HistoryInvoiceModel() {
        this.id = -1;
        this.date = null;
        this.total = 0;
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
        this.userId = user.getId();
    }

    public ArrayList<HistoryInvoiceDetailModel> getHisInvoiceDetail() {
        return hisInvoices;
    }

    public void setHisInvoice(ArrayList<HistoryInvoiceDetailModel> hisInvoice) {
        this.hisInvoices = hisInvoice;
    }

    @Override
    public String toString() {
        return "HistoryInvoiceModel{" + "id=" + id + ", date=" + date + ", total=" + total + ", userId=" + userId + ", user=" + user + ", hisInvoices=" + hisInvoices + '}';
    }

    public void addInvoiceDetail(HistoryInvoiceDetailModel hisInvoice) {
        hisInvoices.add(hisInvoice);
        calculateTotal();
    }

    public void addInvoiceDetail(MaterialModel material, int qty, HistoryInvoiceDetailModel hisInvoice) {
        HistoryInvoiceDetailModel hi;
        hi = new HistoryInvoiceDetailModel(material.getId(), material.getKey(), material.getName(),
                material.getUnit(), material.getPrice(), qty, hisInvoice.getMaterialTotal(), hisInvoice.getMaterialCompany(), -1);
        hisInvoices.add(hi);
        calculateTotal();
    }
    
    public void addInvoiceDetail(MaterialModel matModel, int qty) {
        HistoryInvoiceDetailModel hi = new HistoryInvoiceDetailModel(matModel.getId(), matModel.getKey(), matModel.getName(),
                matModel.getUnit(), matModel.getPrice(), qty, qty * (int) matModel.getPrice(), "", -1);
        hisInvoices.add(hi);
        calculateTotal();
    }
    
    public void delInvoiceDetail(HistoryInvoiceDetailModel hisInvoice) {
        hisInvoices.remove(hisInvoice);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (HistoryInvoiceDetailModel hd : hisInvoices) {
            total += hd.getMaterialTotal();
            totalQty += hd.getMaterialQty();
        }
        //this.totalQty = totalQty;
        this.total = total;
    }

    public static HistoryInvoiceModel fromRS(ResultSet rs) {
        HistoryInvoiceModel hisInvoice = new HistoryInvoiceModel();
        try {
            hisInvoice.setId(rs.getInt("material_invoice_id"));
            hisInvoice.setDate(rs.getDate("material_invoice_date"));
            hisInvoice.setTotal(rs.getFloat("material_invoice_total"));
            hisInvoice.setUserId(rs.getInt("user_id"));

            //Population
            UserDao userDao = new UserDao();
            UserModel user = userDao.get(hisInvoice.getUserId());
            hisInvoice.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(HistoryInvoiceModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return hisInvoice;
    }
}

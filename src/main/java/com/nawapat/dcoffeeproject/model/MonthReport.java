/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author parametsmac
 */
public class MonthReport {
    
    private int total_sales_januray;
    private int total_sales_feb;
    private int total_sales_march;
    private int total_sales_april;
    private int total_sales_may;
    private int total_sales_june;
    private int total_sales_july;
    private int total_sales_august;
    private int total_sales_september;
    private int total_sales_october;
    private int total_sales_november;
    private int total_sales_december;

    public MonthReport(int total_sales_januray, int total_sales_feb, int total_sales_march, int total_sales_april, int total_sales_may, int total_sales_june, int total_sales_july, int total_sales_august, int total_sales_september, int total_sales_october, int total_sales_november, int total_sales_december) {
        this.total_sales_januray = total_sales_januray;
        this.total_sales_feb = total_sales_feb;
        this.total_sales_march = total_sales_march;
        this.total_sales_april = total_sales_april;
        this.total_sales_may = total_sales_may;
        this.total_sales_june = total_sales_june;
        this.total_sales_july = total_sales_july;
        this.total_sales_august = total_sales_august;
        this.total_sales_september = total_sales_september;
        this.total_sales_october = total_sales_october;
        this.total_sales_november = total_sales_november;
        this.total_sales_december = total_sales_december;
    }
    
    public MonthReport() {
        this.total_sales_januray = 0;
        this.total_sales_feb = 0;
        this.total_sales_march = 0;
        this.total_sales_april = 0;
        this.total_sales_may = 0;
        this.total_sales_june = 0;
        this.total_sales_july = 0;
        this.total_sales_august = 0;
        this.total_sales_september = 0;
        this.total_sales_october = 0;
        this.total_sales_november = 0;
        this.total_sales_december = 0;
    }

    public int getTotal_sales_januray() {
        return total_sales_januray;
    }

    public void setTotal_sales_januray(int total_sales_januray) {
        this.total_sales_januray = total_sales_januray;
    }

    public int getTotal_sales_feb() {
        return total_sales_feb;
    }

    public void setTotal_sales_feb(int total_sales_feb) {
        this.total_sales_feb = total_sales_feb;
    }

    public int getTotal_sales_march() {
        return total_sales_march;
    }

    public void setTotal_sales_march(int total_sales_march) {
        this.total_sales_march = total_sales_march;
    }

    public int getTotal_sales_april() {
        return total_sales_april;
    }

    public void setTotal_sales_april(int total_sales_april) {
        this.total_sales_april = total_sales_april;
    }

    public int getTotal_sales_may() {
        return total_sales_may;
    }

    public void setTotal_sales_may(int total_sales_may) {
        this.total_sales_may = total_sales_may;
    }

    public int getTotal_sales_june() {
        return total_sales_june;
    }

    public void setTotal_sales_june(int total_sales_june) {
        this.total_sales_june = total_sales_june;
    }

    public int getTotal_sales_july() {
        return total_sales_july;
    }

    public void setTotal_sales_july(int total_sales_july) {
        this.total_sales_july = total_sales_july;
    }

    public int getTotal_sales_august() {
        return total_sales_august;
    }

    public void setTotal_sales_august(int total_sales_august) {
        this.total_sales_august = total_sales_august;
    }

    public int getTotal_sales_september() {
        return total_sales_september;
    }

    public void setTotal_sales_september(int total_sales_september) {
        this.total_sales_september = total_sales_september;
    }

    public int getTotal_sales_october() {
        return total_sales_october;
    }

    public void setTotal_sales_october(int total_sales_october) {
        this.total_sales_october = total_sales_october;
    }

    public int getTotal_sales_november() {
        return total_sales_november;
    }

    public void setTotal_sales_november(int total_sales_november) {
        this.total_sales_november = total_sales_november;
    }

    public int getTotal_sales_december() {
        return total_sales_december;
    }

    public void setTotal_sales_december(int total_sales_december) {
        this.total_sales_december = total_sales_december;
    }

    @Override
    public String toString() {
        return "MonthReport{" + "total_sales_januray=" + total_sales_januray + ", total_sales_feb=" + total_sales_feb + ", total_sales_march=" + total_sales_march + ", total_sales_april=" + total_sales_april + ", total_sales_may=" + total_sales_may + ", total_sales_june=" + total_sales_june + ", total_sales_july=" + total_sales_july + ", total_sales_august=" + total_sales_august + ", total_sales_september=" + total_sales_september + ", total_sales_october=" + total_sales_october + ", total_sales_november=" + total_sales_november + ", total_sales_december=" + total_sales_december + '}';
    }
    
    
    
    
    
    
    
    
    
    
    public static MonthReport fromRS(ResultSet rs) {
        MonthReport obj = new MonthReport();
        try {
            obj.setTotal_sales_januray(rs.getInt("total_sales_januray"));
            obj.setTotal_sales_feb(rs.getInt("total_sales_feb"));
            obj.setTotal_sales_march(rs.getInt("total_sales_march"));
            obj.setTotal_sales_april(rs.getInt("total_sales_april"));
            obj.setTotal_sales_may(rs.getInt("total_sales_may"));
            obj.setTotal_sales_june(rs.getInt("total_sales_june"));
            obj.setTotal_sales_july(rs.getInt("total_sales_july"));
            obj.setTotal_sales_august(rs.getInt("total_sales_august"));
            obj.setTotal_sales_september(rs.getInt("total_sales_september"));
            obj.setTotal_sales_october(rs.getInt("total_sales_october"));
            obj.setTotal_sales_november(rs.getInt("total_sales_november"));
            obj.setTotal_sales_december(rs.getInt("total_sales_december"));
        } catch (SQLException ex) {
            Logger.getLogger(MonthReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}

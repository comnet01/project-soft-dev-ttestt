/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.HistoryInvoiceDetailDao;
import com.nawapat.dcoffeeproject.model.HistoryInvoiceDetailModel;
import java.util.List;

/**
 *
 * @author Asus
 */
public class HistoryInvoiceDetailService {
    public HistoryInvoiceDetailModel getById(int id) {
        HistoryInvoiceDetailDao hisInvoiceDetailDao = new HistoryInvoiceDetailDao();
        return hisInvoiceDetailDao.get(id);
    }

    public List<HistoryInvoiceDetailModel> getHistoryInvoiceDetails() {
        HistoryInvoiceDetailDao hisInvoiceDetailDao = new HistoryInvoiceDetailDao();
        return hisInvoiceDetailDao.getAll(" material_invoice_detail_id asc");
    }
    
    public List<HistoryInvoiceDetailModel> getHistoryInvoiceDetailsById(int id) {
        HistoryInvoiceDetailDao hisInvoiceDetailDao = new HistoryInvoiceDetailDao();
        return hisInvoiceDetailDao.getAllById(id);
    }
    
    public HistoryInvoiceDetailModel addNew(HistoryInvoiceDetailModel editedHisInvoiceDetail) {
        HistoryInvoiceDetailDao hisInvoiceDetailDao = new HistoryInvoiceDetailDao();
        return hisInvoiceDetailDao.save(editedHisInvoiceDetail);
    }

    public HistoryInvoiceDetailModel update(HistoryInvoiceDetailModel editedHisInvoiceDetail) {
        HistoryInvoiceDetailDao hisInvoiceDetailDao = new HistoryInvoiceDetailDao();
        return hisInvoiceDetailDao.update(editedHisInvoiceDetail);
    }

    public int delete(HistoryInvoiceDetailModel editedHisInvoiceDetail) {
        HistoryInvoiceDetailDao hisInvoiceDetailDao = new HistoryInvoiceDetailDao();
        return hisInvoiceDetailDao.delete(editedHisInvoiceDetail);
    }
}
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.HistoryMatDao;
import com.nawapat.dcoffeeproject.dao.HistoryMatDetailDao;
import com.nawapat.dcoffeeproject.model.HistoryMatDetailModel;

import com.nawapat.dcoffeeproject.model.HistoryMatModel;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class HistoryMatService {
    public HistoryMatModel getById(int id) {
        HistoryMatDao hisMatDao = new HistoryMatDao();
        return hisMatDao.get(id);
    }
            
    public ArrayList<HistoryMatModel> getHistoryMats(){
        HistoryMatDao hisMatDao = new HistoryMatDao();
        return (ArrayList<HistoryMatModel>) hisMatDao.getAll(" history_material_id desc");
    }
    public ArrayList<HistoryMatModel> getHistoryMatsByDate(String begin, String end){
        HistoryMatDao hisMatDao = new HistoryMatDao();
        return (ArrayList<HistoryMatModel>) hisMatDao.getAllbyDate(begin, end,"history_material_id desc");
    }
    
        public ArrayList<HistoryMatDetailModel> getHistoryMatDetails(){
        HistoryMatDetailDao hisMatDetailDao = new HistoryMatDetailDao();
        return (ArrayList<HistoryMatDetailModel>) hisMatDetailDao.getAll(" history_material_detail_id asc");
    }

    public HistoryMatModel addNew(HistoryMatModel editedHistoryMat) {
        HistoryMatDao hisMatDao = new HistoryMatDao();
        HistoryMatDetailDao hisMatDetailDao = new HistoryMatDetailDao();
        HistoryMatModel hisMat = hisMatDao.save(editedHistoryMat);
        for(HistoryMatDetailModel hm: editedHistoryMat.getHisMatDetail()) {
            hm.setHistoryMaterialId(hisMat.getId());
            hisMatDetailDao.save(hm);
        }
        return hisMat;
    }

    public HistoryMatModel update(HistoryMatModel editedHistoryMat) {
        HistoryMatDao hisMatDao = new HistoryMatDao();
        return hisMatDao.update(editedHistoryMat);
    }
    
        public HistoryMatDetailModel updateDetail(HistoryMatDetailModel editedHistoryMatDetail) {
        HistoryMatDetailDao hisMatDetailDao = new HistoryMatDetailDao();
        return hisMatDetailDao.update(editedHistoryMatDetail);
    }

    public int delete(HistoryMatModel editedHistoryMat) {
        HistoryMatDao hisMatDao = new HistoryMatDao();
        return hisMatDao.delete(editedHistoryMat);
    }
    
        public int deleteDetail(HistoryMatDetailModel editedHistoryMatDetail) {
        HistoryMatDetailDao hisMatDetailDao = new HistoryMatDetailDao();
        return hisMatDetailDao.delete(editedHistoryMatDetail);
    }
}

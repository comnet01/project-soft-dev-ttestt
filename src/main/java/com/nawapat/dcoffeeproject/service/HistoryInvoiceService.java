/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.HistoryInvoiceDao;
import com.nawapat.dcoffeeproject.dao.HistoryInvoiceDetailDao;
import com.nawapat.dcoffeeproject.model.HistoryInvoiceDetailModel;
import com.nawapat.dcoffeeproject.model.HistoryInvoiceModel;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class HistoryInvoiceService {
    
    public HistoryInvoiceModel getById(int id) {
        HistoryInvoiceDao hisInvoiceDao = new HistoryInvoiceDao();
        return hisInvoiceDao.get(id);
    }
    
    public ArrayList<HistoryInvoiceModel> getHisInvoices(){
        HistoryInvoiceDao hisInvoiceDao = new HistoryInvoiceDao();
        return (ArrayList<HistoryInvoiceModel>) hisInvoiceDao.getAll(" material_invoice_id desc");
    }
    
    public ArrayList<HistoryInvoiceModel> getHisInvoicesByDate(String begin, String end){
        HistoryInvoiceDao hisInvoiceDao = new HistoryInvoiceDao();
        return (ArrayList<HistoryInvoiceModel>) hisInvoiceDao.getAllByDate(begin, end," material_invoice_id desc");
    }
    
    public ArrayList<HistoryInvoiceDetailModel> getHisInvoiceDetails(){
        HistoryInvoiceDetailDao hisInvoiceDetail = new HistoryInvoiceDetailDao();
        return (ArrayList<HistoryInvoiceDetailModel>) hisInvoiceDetail.getAll(" material_invoice_detail_id asc");
    }
    
    public HistoryInvoiceModel addNew(HistoryInvoiceModel editedHisInvoice) {
        HistoryInvoiceDao hisInvoiceDao = new HistoryInvoiceDao();
        HistoryInvoiceDetailDao hisInvoiceDetailDao = new HistoryInvoiceDetailDao();
        HistoryInvoiceModel hisInvoice = hisInvoiceDao.save(editedHisInvoice);
        for(HistoryInvoiceDetailModel hi: editedHisInvoice.getHisInvoiceDetail()) {
            hi.setMaterialInvoiceId(hisInvoice.getId());
            hisInvoiceDetailDao.save(hi);
        }
        return hisInvoice;
    }

    public HistoryInvoiceModel update(HistoryInvoiceModel editedHisInvoice) {
        HistoryInvoiceDao hisInvoiceDao = new HistoryInvoiceDao();
        return hisInvoiceDao.update(editedHisInvoice);
    }
    
        public HistoryInvoiceDetailModel updateDetail(HistoryInvoiceDetailModel editedHisInvoiceDetail) {
        HistoryInvoiceDetailDao hisInvoiceDetailDao = new HistoryInvoiceDetailDao();
        return hisInvoiceDetailDao.update(editedHisInvoiceDetail);
    }

    public int delete(HistoryInvoiceModel editedHisInvoice) {
        HistoryInvoiceDao hisInvoiceDao = new HistoryInvoiceDao();
        return hisInvoiceDao.delete(editedHisInvoice);
    }
    
        public int deleteDetail(HistoryInvoiceDetailModel editedHisInvoiceDetail) {
        HistoryInvoiceDetailDao hisInvoiceDetailDao = new HistoryInvoiceDetailDao();
        return hisInvoiceDetailDao.delete(editedHisInvoiceDetail);
    }
        
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.MaterialDao;
import com.nawapat.dcoffeeproject.model.MaterialModel;
import java.util.List;

/**
 *
 * @author asus
 */
public class MaterialService {

    public List<MaterialModel> getMaterials() {
        MaterialDao matDao = new MaterialDao();
        return matDao.getAll(" material_key asc");
    }

    public MaterialModel addNew(MaterialModel editedUser) {
        MaterialDao matDao = new MaterialDao();
        return matDao.save(editedUser);
    }

    public MaterialModel update(MaterialModel editedUser) {
        MaterialDao matDao = new MaterialDao();
        return matDao.update(editedUser);
    }

    public int delete(MaterialModel editedUser) {
        MaterialDao matDao = new MaterialDao();
        return matDao.delete(editedUser);
    }

    public MaterialModel clearMat(MaterialModel editedUser) {
        MaterialDao matDao = new MaterialDao();
        return matDao.clearMat(editedUser);
    }
}

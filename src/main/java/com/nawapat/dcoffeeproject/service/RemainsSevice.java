/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.RemainsDao;
import com.nawapat.dcoffeeproject.dao.SalesDao;
import com.nawapat.dcoffeeproject.model.RemainsReport;
import com.nawapat.dcoffeeproject.model.SalesReport;
import java.util.List;

/**
 *
 * @author parametsmac
 */
public class RemainsSevice {

    public List<RemainsReport> getTopTenRemainsByTotalRemains() {
        RemainsDao remainsDao = new RemainsDao();
        return remainsDao.getRemainByTotalRemain(10);
    }

//    public List<SalesReport> getTopTenArtistByTotalPrice(String begin, String end) {
//        SalesDao salesDao = new SalesDao();
//        return salesDao.getArtistByTotalPrice(begin, end, 10);
//    }
}

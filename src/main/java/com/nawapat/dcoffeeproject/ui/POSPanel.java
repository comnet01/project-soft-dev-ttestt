/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.service.CustomerService;
import com.nawapat.dcoffeeproject.model.CustomerModel;
import com.nawapat.dcoffeeproject.model.ProductModel;
import com.nawapat.dcoffeeproject.model.RecieptDetailModel;
import com.nawapat.dcoffeeproject.model.RecieptModel;
import com.nawapat.dcoffeeproject.service.ProductService;
import com.nawapat.dcoffeeproject.service.RecieptService;
import com.nawapat.dcoffeeproject.service.UserService;
import component.BuyProductable;
import component.BuyProductable2;
import component.InputDialogMember;
import component.MemberDialog;
import component.ProductListPanel;
import component.ProductListPanel2;
import component.QrDialog;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PrinterException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author kornn
 */
public class POSPanel extends javax.swing.JPanel implements BuyProductable, BuyProductable2 {

    private final CustomerService customerService;
    private CustomerModel editedCustomer;
    private List<CustomerModel> list;
    private ProductListPanel productListpanel;
    private final ProductListPanel2 productListpanel2;
    ArrayList<ProductModel> products;
    ProductService productservice = new ProductService();
    RecieptService recieptService = new RecieptService();
    RecieptModel reciept;
    private List<ProductModel> productList;
    private AbstractTableModel model;
    RecieptDetailModel recieptDetail = new RecieptDetailModel();

    /**
     * Creates new form POSPanel
     */
    public POSPanel() {
        initComponents();
        CloseAll();
        txtBill.setText("");
        customerService = new CustomerService();
        list = customerService.getCustomers();
        Color bgColor1 = new Color(255, 251, 233);
        jTableOrder.setBackground(bgColor1);
        productListpanel = new ProductListPanel();
        productListpanel2 = new ProductListPanel2();
        productListpanel.addOnBuyProduct(this);
        productListpanel2.addOnBuyProduct(this);
        scrList1.setViewportView(productListpanel);
        scrList2.setViewportView(productListpanel2);
        reciept = new RecieptModel();
        reciept.setUser(UserService.getCurrentUser());
        model = new AbstractTableModel() {
            String[] headers = {"Name", "Price", "Size", "SweetLevel", "Qty", "Total"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return reciept.getRecieptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<RecieptDetailModel> recieptDetails = reciept.getRecieptDetails();
                RecieptDetailModel recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getProductName();
                    case 1:
                        return recieptDetail.getProductPrice();
                    case 2:
                        return recieptDetail.getProductSize();
                    case 3:
                        return recieptDetail.getProductSweetLevel();
                    case 4:
                        return recieptDetail.getQty();
                    case 5:
                        return recieptDetail.getTotalPrice();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<RecieptDetailModel> recieptDetails = reciept.getRecieptDetails();
                RecieptDetailModel recieptDetail = recieptDetails.get(rowIndex);
                if (columnIndex == 4) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    recieptDetail.setQty(qty);
                    reciept.calculateTotal();
                    refreshReciept();
                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 4:
                        return true;
                    default:
                        return false;
                }
            }

        };
        jTableOrder.setModel(model);
    }

    private void openMemberDialogButtonActionPerformed(java.awt.event.ActionEvent evt) {
        MemberDialog memberDialog = new MemberDialog((JFrame) SwingUtilities.getWindowAncestor(this), editedCustomer, jTFNameMem, jTFpoint, jTFIdMem, jTFKeyMem, jTFTelMem);
        memberDialog.setVisible(true);
    }

    public JTextField getTxtName() {
        return jTFNameMem;
    }

    public void setTxtName(JTextField jTFNameMem) {
        this.jTFNameMem = jTFNameMem;
    }

    public JTextField getTxtPoint() {
        return jTFpoint;
    }

    public void setTxtPoint(JTextField jTFpoint) {
        this.jTFpoint = jTFpoint;
    }

    public JTextField getjTFsearchProduct() {
        return jTFsearchProduct;
    }

    private void refreshReciept() {
        jTableOrder.revalidate();
        jTableOrder.repaint();
        jTFSubTotal.setText(Float.toString(reciept.getTotal()));
        jTFtotal.setText(Float.toString(reciept.getTotal()));
        jTFReceive.setText(Float.toString(reciept.getCash()));
        jTFChange.setText(Integer.toString(0));
    }

    private void ClearReciept() {
        jTableOrder.revalidate();
        jTableOrder.repaint();
        jTFSubTotal.setText(Float.toString(0));
        jTFtotal.setText(Float.toString(0));
        jTFDiscount.setText(Integer.toString(0));
        jTFReceive.setText(Float.toString(0));
        jTFChange.setText(Integer.toString(0));
    }

    public void CloseAll() {
        jTFusePoint.setText("0");
        jTFproDiscount.setText("0");
        jTFDiscount.setText("0");
        jTFSubTotal.setText("0.00");
        jTFtotal.setText("0.00");
        jbtnPay.setEnabled(false);
        jTFSubTotal.setEnabled(false);
        jTFtotal.setEnabled(false);
        jTFReceive.setEnabled(false);
        jTFChange.setEnabled(false);
        jbtnResetPro.setEnabled(false);
        jTFproDiscount.setEnabled(false);
        jTFDiscount.setEnabled(false);
        jTFSubTotal.setEnabled(false);
        jTFtotal.setEnabled(false);
        jTFChange.setEnabled(false);
        jTFNameMem.setEnabled(false);
        jTFpoint.setEnabled(false);
        jTFIdMem.setEnabled(false);
        jTFusePoint.setEnabled(false);
        jComboPayMethod.setEnabled(false);
        jbtnPay.setEnabled(false);
    }

    public void OpenAll() {
        jbtnPay.setEnabled(true);
        jbtnRemove.setEnabled(true);
        jbtnClearAll.setEnabled(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")

//    -------------------------------------------------FuncTion All Here------------------------------------------------------
    public void calChange() {
        double cTotal = Double.parseDouble(jTFtotal.getText());
        double Receive = Double.parseDouble(jTFReceive.getText());
        double cChangeMoney = Receive - cTotal;
        String FChange = String.format("%.2f", cChangeMoney);
        jTFChange.setText(FChange);
    }

    public void PromoUpdate() throws NumberFormatException {
        double sum = Double.parseDouble(jTFSubTotal.getText());
        double discount = Double.parseDouble(jTFDiscount.getText());
        double cChange = (sum - discount);
        jTFtotal.setText(Double.toString(cChange));
    }

    public void bill() {
        txtUsername.setText(UserService.getCurrentUser().getEmployee().getName());
        String subTotal = jTFSubTotal.getText();
        String discount = jTFDiscount.getText();
        String total = jTFtotal.getText();
        String Cash = jTFReceive.getText();
        String Change = jTFChange.getText();
        String User = txtUsername.getText();
        if (jComboPayMethod.getSelectedItem().equals("Cash")) {
            txtPayMethod.setText("Cash");
        } else if (jComboPayMethod.getSelectedItem().equals("Qr")) {
            txtPayMethod.setText("Qr Code");
        }
        String paymentMethod = txtPayMethod.getText();
        if (jComboPro.getSelectedItem().equals("Member Point")) {
            txtUsePro4Bill.setText("Member Point");
            if (!jTFusePoint.getText().isEmpty()) {
                txtUsePoint.setText(jTFusePoint.getText());
            } else {
                txtUsePoint.setText("0");
            }
        } else if (jComboPro.getSelectedItem().equals("Mother Day")) {
            txtUsePro4Bill.setText("Mother Day");
        } else if (jComboPro.getSelectedItem().equals("Father Day")) {
            txtUsePro4Bill.setText("Father Day");
        } else if (jComboPro.getSelectedItem().equals("Christmas")) {
            txtUsePro4Bill.setText("Christmas");
        } else if (jComboPro.getSelectedItem().equals("Valentine")) {
            txtUsePro4Bill.setText("Valentine");
        } else if (jComboPro.getSelectedItem().equals("Halloween")) {
            txtUsePro4Bill.setText("Halloween");
        } else if (jComboPro.getSelectedItem().equals("Not Use")) {
            txtUsePro4Bill.setText("Not Use");
        }
        String namePromotion = txtUsePro4Bill.getText();

        if (!jTFNameMem.getText().isEmpty()) {
            txtNameMember.setText(jTFNameMem.getText());
            txtCurrentPoint.setText(jTFpoint.getText());
            txtUsePoint.setText(jTFusePoint.getText());
        } else {
            txtNameMember.setText(" - ");
            txtCurrentPoint.setText(" - ");
            txtUsePoint.setText(" - ");
        }
        String nameMemberForBill = txtNameMember.getText();
        String curPoint = txtCurrentPoint.getText();
        String usePoint = txtUsePoint.getText();

        Calendar timer = Calendar.getInstance();
        Date currentTime = timer.getTime();
        SimpleDateFormat tTime = new SimpleDateFormat("HH:mm:ss");
        String formattedTime = tTime.format(currentTime);
        SimpleDateFormat tDate = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = tDate.format(currentTime);

        //Starter
        model = (AbstractTableModel) jTableOrder.getModel();
        txtBill.setText(txtBill.getText() + "*****************************************************************************************************\n");
        txtBill.setText(txtBill.getText() + "\n");
        txtBill.setText(txtBill.getText() + "                                                            DCoffee                                              \n");
        txtBill.setText(txtBill.getText() + "\n");
        txtBill.setText(txtBill.getText() + "*****************************************************************************************************\n");
        txtBill.setText(txtBill.getText() + "\n");

        //Heading
        txtBill.setText(txtBill.getText() + "Staff: " + User + "\n");
        txtBill.setText(txtBill.getText() + "Date: " + formattedDate + "\t\t\t" + "Time: " + formattedTime + "\n");
        txtBill.setText(txtBill.getText() + "Member Name: " + nameMemberForBill + "\n");
        txtBill.setText(txtBill.getText() + "Currrent Point: " + curPoint + "\t\t\t" + "Use Point: " + usePoint + "\n");
        txtBill.setText(txtBill.getText() + "\n");
        txtBill.setText(txtBill.getText() + "Product" + "\t" +" "+" "+ "Price" + "\t" + "Size" + "\t" + "SweetLevel" + "\t" + "Quantity" + "\t" + "Total" + "\n");
        txtBill.setText(txtBill.getText() + "------------------------------------------------------------------------------------------------------------------------------\n");
        for (int i = 0; i < model.getRowCount(); i++) {
            String pName = (String) model.getValueAt(i, 0);
            Float fPrice = (Float) model.getValueAt(i, 1);
            String price = Float.toString(fPrice);
            String pSize = (String) model.getValueAt(i, 2);
            String pSwl = (String) model.getValueAt(i, 3);
            int iQty = (int) model.getValueAt(i, 4);
            String pQty = Integer.toString(iQty);
            Float fTotal = (Float) model.getValueAt(i, 5);
            String pTotal = Float.toString(fTotal);
            txtBill.setText(txtBill.getText() + pName + "\t" +" "+" "+ price + "\t" + pSize + "\t" + pSwl + "\t" + pQty + "\t" + pTotal + "\n");
        }
        txtBill.setText(txtBill.getText() + "------------------------------------------------------------------------------------------------------------------------------\n");
        txtBill.setText(txtBill.getText() + "\n");

        txtBill.setText(txtBill.getText() + "Payment Method: " + "\t" + paymentMethod + "\n");
        txtBill.setText(txtBill.getText() + "Subtotal: " + "\t\t" + subTotal + "\n");
        txtBill.setText(txtBill.getText() + "Promotion: " + "\t\t" + namePromotion + "\n");
        txtBill.setText(txtBill.getText() + "Discount: " + "\t\t" + discount + "\n");
        txtBill.setText(txtBill.getText() + "Total: " + "\t\t" + total + "\n");
        txtBill.setText(txtBill.getText() + "Cash: " + "\t\t" + Cash + "\n");
        txtBill.setText(txtBill.getText() + "Change: " + "\t\t" + Change + "\n");
        txtBill.setText(txtBill.getText() + "\n");
        txtBill.setText(txtBill.getText() + "================================================================================================================================\n");
        txtBill.setText(txtBill.getText() + "\n");
        txtBill.setText(txtBill.getText() + "\t" + "\t" + "Thank you, Please Come Again.");
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTFsearchProduct = new javax.swing.JTextField();
        txtUsername = new javax.swing.JTextField();
        txtPayMethod = new javax.swing.JTextField();
        txtUsePro4Bill = new javax.swing.JTextField();
        txtNameMember = new javax.swing.JTextField();
        txtCurrentPoint = new javax.swing.JTextField();
        txtUsePoint = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtBill = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jTP1 = new javax.swing.JTabbedPane();
        jPanel = new javax.swing.JPanel();
        scrList1 = new javax.swing.JScrollPane();
        jPanel9 = new javax.swing.JPanel();
        scrList2 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableOrder = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTFSubTotal = new javax.swing.JTextField();
        jTFDiscount = new javax.swing.JTextField();
        jTFtotal = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTFReceive = new javax.swing.JTextField();
        jTFChange = new javax.swing.JTextField();
        jbtnPay = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jbtnRemove = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jComboPayMethod = new javax.swing.JComboBox<>();
        jbtnClearAll = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jbtnAddMem = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jTFNameMem = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTFpoint = new javax.swing.JTextField();
        btnChooseMember = new javax.swing.JButton();
        jTFIdMem = new javax.swing.JTextField();
        jTFKeyMem = new javax.swing.JTextField();
        jTFTelMem = new javax.swing.JTextField();
        jTFusePoint = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jTFproDiscount = new javax.swing.JTextField();
        jComboPro = new javax.swing.JComboBox<>();
        jbtnResetPro = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(238, 214, 196));

        jPanel2.setBackground(new java.awt.Color(255, 243, 228));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(72, 52, 52));
        jLabel1.setText("DCoffee");

        jTFsearchProduct.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFsearchProduct.setForeground(new java.awt.Color(72, 52, 52));
        jTFsearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFsearchProductActionPerformed(evt);
            }
        });
        jTFsearchProduct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFsearchProductKeyReleased(evt);
            }
        });

        txtBill.setColumns(20);
        txtBill.setRows(5);
        jScrollPane2.setViewportView(txtBill);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jTFsearchProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addComponent(txtUsePoint, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(70, 70, 70)
                        .addComponent(txtCurrentPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(122, 122, 122)
                        .addComponent(txtNameMember, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(107, 107, 107)
                        .addComponent(txtUsePro4Bill, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(111, 111, 111)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(70, 70, 70)
                .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPayMethod, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(185, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFsearchProduct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPayMethod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUsePro4Bill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNameMember, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCurrentPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUsePoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );

        jPanel3.setBackground(new java.awt.Color(107, 79, 79));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jTP1.setBackground(new java.awt.Color(255, 255, 255));
        jTP1.setForeground(new java.awt.Color(72, 52, 52));
        jTP1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jPanel.setBackground(new java.awt.Color(255, 255, 255));
        jPanel.setForeground(new java.awt.Color(72, 52, 52));

        scrList1.setBackground(new java.awt.Color(255, 255, 255));
        scrList1.setForeground(new java.awt.Color(72, 52, 52));

        javax.swing.GroupLayout jPanelLayout = new javax.swing.GroupLayout(jPanel);
        jPanel.setLayout(jPanelLayout);
        jPanelLayout.setHorizontalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrList1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 568, Short.MAX_VALUE)
        );
        jPanelLayout.setVerticalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrList1, javax.swing.GroupLayout.DEFAULT_SIZE, 348, Short.MAX_VALUE)
        );

        jTP1.addTab("Drink", jPanel);

        jPanel9.setBackground(new java.awt.Color(255, 204, 153));
        jPanel9.setForeground(new java.awt.Color(72, 52, 52));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrList2, javax.swing.GroupLayout.DEFAULT_SIZE, 568, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrList2, javax.swing.GroupLayout.DEFAULT_SIZE, 348, Short.MAX_VALUE)
        );

        jTP1.addTab("Cake", jPanel9);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTP1)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTP1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jPanel4.setBackground(new java.awt.Color(238, 214, 196));
        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setForeground(new java.awt.Color(72, 52, 52));

        jTableOrder.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jTableOrder.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jTableOrder.setForeground(new java.awt.Color(72, 52, 52));
        jTableOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Price", "Size", "SweetLevel", "Quantity", "Total"
            }
        ));
        jScrollPane1.setViewportView(jTableOrder);

        jPanel5.setBackground(new java.awt.Color(255, 243, 228));
        jPanel5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel5.setForeground(new java.awt.Color(72, 52, 52));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(72, 52, 52));
        jLabel4.setText("SubTotal");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(72, 52, 52));
        jLabel5.setText("Discount");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(72, 52, 52));
        jLabel6.setText("Total");

        jTFSubTotal.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFSubTotal.setForeground(new java.awt.Color(72, 52, 52));
        jTFSubTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFSubTotalActionPerformed(evt);
            }
        });

        jTFDiscount.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFDiscount.setForeground(new java.awt.Color(72, 52, 52));
        jTFDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFDiscountActionPerformed(evt);
            }
        });

        jTFtotal.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFtotal.setForeground(new java.awt.Color(72, 52, 52));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTFtotal))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTFDiscount))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTFSubTotal)))
                .addGap(12, 12, 12))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(10, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTFSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTFDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTFtotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16))
        );

        jPanel6.setBackground(new java.awt.Color(255, 243, 228));
        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel6.setForeground(new java.awt.Color(72, 52, 52));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(72, 52, 52));
        jLabel2.setText("Receive");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(72, 52, 52));
        jLabel3.setText("Change ");

        jTFReceive.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFReceive.setForeground(new java.awt.Color(72, 52, 52));
        jTFReceive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFReceiveActionPerformed(evt);
            }
        });
        jTFReceive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFReceiveKeyReleased(evt);
            }
        });

        jTFChange.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFChange.setForeground(new java.awt.Color(72, 52, 52));

        jbtnPay.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jbtnPay.setForeground(new java.awt.Color(72, 52, 52));
        jbtnPay.setText("Pay");
        jbtnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnPayActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addGap(42, 42, 42)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTFReceive)
                            .addComponent(jTFChange)))
                    .addComponent(jbtnPay, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTFReceive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTFChange, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jbtnPay)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(72, 52, 52));
        jLabel9.setText("Current Order");

        jPanel11.setBackground(new java.awt.Color(255, 243, 228));
        jPanel11.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel11.setForeground(new java.awt.Color(72, 52, 52));

        jbtnRemove.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jbtnRemove.setForeground(new java.awt.Color(72, 52, 52));
        jbtnRemove.setText("Remove");
        jbtnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnRemoveActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(72, 52, 52));
        jLabel8.setText("Payment Method");

        jComboPayMethod.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Method", "Cash", "Qr" }));
        jComboPayMethod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboPayMethodActionPerformed(evt);
            }
        });

        jbtnClearAll.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jbtnClearAll.setForeground(new java.awt.Color(72, 52, 52));
        jbtnClearAll.setText("Clear");
        jbtnClearAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnClearAllActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jComboPayMethod, 0, 92, Short.MAX_VALUE)
                .addGap(86, 86, 86)
                .addComponent(jbtnRemove)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtnClearAll, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtnRemove, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8)
                    .addComponent(jComboPayMethod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtnClearAll))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(12, 12, 12)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel9)
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel7.setBackground(new java.awt.Color(255, 243, 228));
        jPanel7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jbtnAddMem.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jbtnAddMem.setForeground(new java.awt.Color(72, 52, 52));
        jbtnAddMem.setText("Add Member");
        jbtnAddMem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnAddMemActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(72, 52, 52));
        jLabel10.setText("Name");

        jTFNameMem.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFNameMem.setForeground(new java.awt.Color(72, 52, 52));
        jTFNameMem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFNameMemActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(72, 52, 52));
        jLabel11.setText("Point");

        jTFpoint.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFpoint.setForeground(new java.awt.Color(72, 52, 52));

        btnChooseMember.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnChooseMember.setForeground(new java.awt.Color(72, 52, 52));
        btnChooseMember.setText("Choose Member");
        btnChooseMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChooseMemberActionPerformed(evt);
            }
        });

        jTFIdMem.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFIdMem.setForeground(new java.awt.Color(72, 52, 52));
        jTFIdMem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFIdMemActionPerformed(evt);
            }
        });

        jTFKeyMem.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFKeyMem.setForeground(new java.awt.Color(72, 52, 52));
        jTFKeyMem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFKeyMemActionPerformed(evt);
            }
        });

        jTFTelMem.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFTelMem.setForeground(new java.awt.Color(72, 52, 52));
        jTFTelMem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFTelMemActionPerformed(evt);
            }
        });

        jTFusePoint.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFusePoint.setForeground(new java.awt.Color(72, 52, 52));
        jTFusePoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFusePointActionPerformed(evt);
            }
        });
        jTFusePoint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFusePointKeyReleased(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(72, 52, 52));
        jLabel13.setText("Enter Point");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTFNameMem, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTFIdMem, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(jTFpoint, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jTFKeyMem, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62)
                        .addComponent(jTFTelMem, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChooseMember))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTFusePoint, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jbtnAddMem, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnChooseMember)
                            .addComponent(jTFNameMem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTFIdMem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTFKeyMem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTFTelMem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jTFpoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFusePoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtnAddMem)
                    .addComponent(jLabel13))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 243, 228));
        jPanel8.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(72, 52, 52));
        jLabel12.setText("Select Promotion to apply ( Limit 1 per order )");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(72, 52, 52));
        jLabel7.setText("Discount");

        jTFproDiscount.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTFproDiscount.setForeground(new java.awt.Color(72, 52, 52));
        jTFproDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFproDiscountActionPerformed(evt);
            }
        });

        jComboPro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jComboPro.setForeground(new java.awt.Color(72, 52, 52));
        jComboPro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-", "Not Use", "Member Point", "Mother Day", "Father Day", "Christmas", "Valentine", "Halloween" }));
        jComboPro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboProActionPerformed(evt);
            }
        });

        jbtnResetPro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jbtnResetPro.setForeground(new java.awt.Color(72, 52, 52));
        jbtnResetPro.setText("Reset Promotion");
        jbtnResetPro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnResetProActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jComboPro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbtnResetPro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTFproDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(jTFproDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jComboPro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jbtnResetPro)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1081, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 620, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTFSubTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFSubTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTFSubTotalActionPerformed

    private void jTFDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFDiscountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTFDiscountActionPerformed

    private void jTFReceiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFReceiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTFReceiveActionPerformed

    private void jbtnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnRemoveActionPerformed
        int selectedDel = jTableOrder.getSelectedRow();
        ArrayList<RecieptDetailModel> recieptDetails = reciept.getRecieptDetails();
        RecieptDetailModel recieptDetail = recieptDetails.get(selectedDel);
        if (selectedDel >= 0) {
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete " + recieptDetail.getProductName() + "?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                reciept.delRecieptDetail(recieptDetail);
                refreshReciept();
            }
        }
    }//GEN-LAST:event_jbtnRemoveActionPerformed

    private void jbtnClearAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnClearAllActionPerformed
        ArrayList<RecieptDetailModel> recieptDetails = reciept.getRecieptDetails();
        int input = JOptionPane.showConfirmDialog(this,
                "Do you want to Clear ?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        if (input == 0) {
            recieptDetails.clear();
            ClearAll();
            ClearReciept();
        }

    }//GEN-LAST:event_jbtnClearAllActionPerformed

    private void ClearAll() {
        CloseAll();
        jTFReceive.setText("");
        jTFChange.setText("");
        jTFSubTotal.setText("0.00");
        jTFDiscount.setText("0");
        jTFtotal.setText("0.00");
        jTFIdMem.setText("");
        jTFNameMem.setText("");
        jTFTelMem.setText("");
        jTFpoint.setText("");
        jTFusePoint.setText("");
        jComboPro.setSelectedIndex(0);
        jComboPayMethod.setSelectedIndex(0);
        jTFproDiscount.setText("0");
        jbtnPay.setEnabled(false);
        jComboPayMethod.setEnabled(false);
        jbtnResetPro.setEnabled(false);
        jComboPro.setEnabled(true);
        jTFReceive.setEnabled(false);
        jComboPro.setEnabled(true);
        jTFsearchProduct.setText("");
        String searchText = getjTFsearchProduct().getText();
        productListpanel.searchAndUpdateList(searchText);
        productListpanel2.searchAndUpdateList(searchText);
    }

    private void jbtnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnPayActionPerformed
        jTFReceive.setEnabled(false);
        reciept.setCash(Float.parseFloat(jTFReceive.getText()));
        if (!jTFNameMem.getText().isEmpty() && Float.parseFloat(jTFSubTotal.getText()) >= 50) {
            reciept.setCustomerId(Integer.parseInt(jTFIdMem.getText()));
            float subTt = Float.parseFloat(jTFtotal.getText());
            float bonusPoint = ((subTt / 50) * 5);
            int points = (int) (Integer.parseInt(jTFpoint.getText()) + bonusPoint);
            setFormToObject();
            editedCustomer.setPoint(points);
            customerService.update(editedCustomer);
            if (!jTFusePoint.getText().isEmpty()) {
                int oldPoint = editedCustomer.getPoint();
                int usePoint = Integer.parseInt(jTFusePoint.getText());
                int newPoint = oldPoint - usePoint;
                setFormToObject();
                editedCustomer.setPoint(newPoint);
                customerService.update(editedCustomer);
            }
            if (jComboPayMethod.getSelectedItem().equals("Cash")) {
                print();
            } else if (jComboPayMethod.getSelectedItem().equals("Qr")) {
                openQrDialog();
                print();
            }
        } else {
            if (jComboPayMethod.getSelectedItem().equals("Cash")) {
                print();
            } else if (jComboPayMethod.getSelectedItem().equals("Qr")) {
                openQrDialog();
                print();
            }
        }
        recieptService.addNew(reciept);
        calChange();
        clearReciept();
        ClearAll();
        jTFusePoint.setText("0");
        txtBill.setText("");
        jTP1.setSelectedIndex(0);
    }//GEN-LAST:event_jbtnPayActionPerformed

    private void setFormToObject() {
        editedCustomer.setKey(jTFKeyMem.getText());
        editedCustomer.setName(jTFNameMem.getText());
        editedCustomer.setId(Integer.parseInt(jTFIdMem.getText()));
        editedCustomer.setPhone(jTFTelMem.getText());
    }

    private void clearReciept() {
        reciept = new RecieptModel();
        reciept.setUser(UserService.getCurrentUser());
        refreshReciept();
    }

    private void jTFproDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFproDiscountActionPerformed

    }//GEN-LAST:event_jTFproDiscountActionPerformed

    private void jComboProActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboProActionPerformed
        jComboPayMethod.setEnabled(true);
        comboPromotion();
        forPromo();
    }//GEN-LAST:event_jComboProActionPerformed

    private void comboPromotion() {
        if (jComboPro.getSelectedItem().equals("Member Point")) {
            jTFusePoint.setEnabled(true);
            int pointsUsed = Integer.parseInt(jTFpoint.getText());
            if (Float.parseFloat(jTFSubTotal.getText()) >= 100 && pointsUsed >= 10) {
                int usePoint = Integer.parseInt(jTFusePoint.getText());
                jTFproDiscount.setText(Integer.toString(usePoint));
                jTFDiscount.setText(Integer.toString(usePoint));
                if (usePoint > pointsUsed) {
                    int input = JOptionPane.showConfirmDialog(this,
                            "Please Enter point again!!!", "Caution!!", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE);
                    if (input == 0) {
                        jTFusePoint.setText("0");
                        jTFproDiscount.setText("0");
                        jTFDiscount.setText("0");
                        PromoUpdate();
                        jTFusePoint.requestFocus();
                    } else {
                        jTFusePoint.setText("0");
                        jTFproDiscount.setText("0");
                        jTFDiscount.setText("0");
                        PromoUpdate();
                        jTFusePoint.requestFocus();
                    }
                } else if (Integer.parseInt(jTFusePoint.getText()) == 0) {
                    int input = JOptionPane.showConfirmDialog(this,
                            "Please Enter point again!!!", "Caution!!", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE);
                    if (input == 0) {
                        jTFusePoint.setText("0");
                        jTFproDiscount.setText("0");
                        jTFDiscount.setText("0");
                        PromoUpdate();
                        jTFusePoint.requestFocus();
                    } else {
                        jTFusePoint.setText("0");
                        jTFproDiscount.setText("0");
                        jTFDiscount.setText("0");
                        PromoUpdate();
                        jTFusePoint.requestFocus();
                    }
                }
            }
        } else if (jComboPro.getSelectedItem().equals("Mother Day")) {
            jTFproDiscount.setText("-10");
            jTFDiscount.setText("10");
            PromoUpdate();
            jTFusePoint.setEnabled(false);
            jTFusePoint.setText("0");
        } else if (jComboPro.getSelectedItem().equals("Father Day")) {
            jTFproDiscount.setText("-10");
            jTFDiscount.setText("10");
            PromoUpdate();
            jTFusePoint.setEnabled(false);
            jTFusePoint.setText("0");
        } else if (jComboPro.getSelectedItem().equals("Christmas")) {
            jTFproDiscount.setText("-20");
            jTFDiscount.setText("20");
            PromoUpdate();
            jTFusePoint.setEnabled(false);
            jTFusePoint.setText("0");
        } else if (jComboPro.getSelectedItem().equals("Valentine")) {
            jTFproDiscount.setText("-20");
            jTFDiscount.setText("20");
            PromoUpdate();
            jTFusePoint.setEnabled(false);
            jTFusePoint.setText("0");
        } else if (jComboPro.getSelectedItem().equals("Halloween")) {
            jTFproDiscount.setText("-20");
            jTFDiscount.setText("20");
            PromoUpdate();
            jTFusePoint.setEnabled(false);
            jTFusePoint.setText("0");
        } else if (jComboPro.getSelectedItem().equals("Not Use")) {
            jTFproDiscount.setText("0");
            jTFDiscount.setText("0");
            PromoUpdate();
            jTFusePoint.setEnabled(false);
            jTFusePoint.setText("0");
        }
    }

    private void forPromo() throws NumberFormatException {
        OpenAll();
        jbtnResetPro.setEnabled(true);
        jComboPro.setEnabled(false);
        PromoUpdate();
    }

    private void jbtnResetProActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnResetProActionPerformed
        jbtnResetPro.setEnabled(false);

        jComboPro.setEnabled(true);
    }//GEN-LAST:event_jbtnResetProActionPerformed

    private void print() {
        bill();
        try {
            txtBill.print();
        } catch (PrinterException e) {
            System.err.format("Cannot be Print !", e.getMessage());
        }
        txtBill.setText("");
    }

    private void jbtnAddMemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnAddMemActionPerformed
        editedCustomer = new CustomerModel();
        openDialog1();
    }//GEN-LAST:event_jbtnAddMemActionPerformed

    private void btnChooseMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChooseMemberActionPerformed
        editedCustomer = new CustomerModel();
        openDialog();
    }//GEN-LAST:event_btnChooseMemberActionPerformed

    private void jTFsearchProductKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFsearchProductKeyReleased
        String searchText = getjTFsearchProduct().getText();
        productListpanel.searchAndUpdateList(searchText);
        productListpanel2.searchAndUpdateList(searchText);
    }//GEN-LAST:event_jTFsearchProductKeyReleased

    private void jTFsearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFsearchProductActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTFsearchProductActionPerformed

    private void jTFNameMemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFNameMemActionPerformed

    }//GEN-LAST:event_jTFNameMemActionPerformed

    private void jTFIdMemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFIdMemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTFIdMemActionPerformed

    private void jTFReceiveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFReceiveKeyReleased
        calChange();
    }//GEN-LAST:event_jTFReceiveKeyReleased

    private void jTFKeyMemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFKeyMemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTFKeyMemActionPerformed

    private void jTFTelMemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFTelMemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTFTelMemActionPerformed

    private void jTFusePointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFusePointActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTFusePointActionPerformed

    private void jTFusePointKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFusePointKeyReleased

    }//GEN-LAST:event_jTFusePointKeyReleased

    private void jComboPayMethodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboPayMethodActionPerformed
        if (jComboPayMethod.getSelectedItem().equals("Method")) {
            jTFReceive.setEnabled(false);
            jbtnPay.setEnabled(false);
        } else {
            jTFReceive.setEnabled(true);
            jbtnPay.setEnabled(true);
            jTFReceive.requestFocus();
        }
    }//GEN-LAST:event_jComboPayMethodActionPerformed

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        MemberDialog memberDialog = new MemberDialog((JFrame) SwingUtilities.getWindowAncestor(this), editedCustomer, jTFNameMem, jTFpoint, jTFIdMem, jTFKeyMem, jTFTelMem);
        memberDialog.setLocationRelativeTo(this);
        memberDialog.setVisible(true);
        memberDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
//                refreshTable();
            }

        });
    }

    private void openDialog1() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        InputDialogMember inputDialog = new InputDialogMember(frame, editedCustomer);
        inputDialog.setLocationRelativeTo(this);
        inputDialog.setVisible(true);
        inputDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
//                refreshTable();
            }

        });
    }

    private void openQrDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        QrDialog qrDialog = new QrDialog(frame, true);
        qrDialog.setLocationRelativeTo(this);
        qrDialog.setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChooseMember;
    private javax.swing.JComboBox<String> jComboPayMethod;
    private javax.swing.JComboBox<String> jComboPro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTFChange;
    private javax.swing.JTextField jTFDiscount;
    private javax.swing.JTextField jTFIdMem;
    private javax.swing.JTextField jTFKeyMem;
    private javax.swing.JTextField jTFNameMem;
    private javax.swing.JTextField jTFReceive;
    private javax.swing.JTextField jTFSubTotal;
    private javax.swing.JTextField jTFTelMem;
    private javax.swing.JTextField jTFpoint;
    private javax.swing.JTextField jTFproDiscount;
    private javax.swing.JTextField jTFsearchProduct;
    private javax.swing.JTextField jTFtotal;
    private javax.swing.JTextField jTFusePoint;
    private javax.swing.JTabbedPane jTP1;
    private javax.swing.JTable jTableOrder;
    private javax.swing.JButton jbtnAddMem;
    private javax.swing.JButton jbtnClearAll;
    private javax.swing.JButton jbtnPay;
    private javax.swing.JButton jbtnRemove;
    private javax.swing.JButton jbtnResetPro;
    private javax.swing.JScrollPane scrList1;
    private javax.swing.JScrollPane scrList2;
    private javax.swing.JTextArea txtBill;
    private javax.swing.JTextField txtCurrentPoint;
    private javax.swing.JTextField txtNameMember;
    private javax.swing.JTextField txtPayMethod;
    private javax.swing.JTextField txtUsePoint;
    private javax.swing.JTextField txtUsePro4Bill;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(ProductModel product, String productSize, String productSweetLevel, int qty) {
        boolean found = false;
        for (RecieptDetailModel recieptDetail : reciept.getRecieptDetails()) {
//            System.out.println("------------------");
//            System.out.println("+ " +recieptDetail.getProductName());
//            System.out.println(recieptDetail.getProductSize());
//            System.out.println(recieptDetail.getProductSweetLevel());
//            System.out.println(recieptDetail.getQty()+1);
//            System.out.println("------------------");

            if (recieptDetail.getProductName().equals(product.getName())
                    && recieptDetail.getProductSize().equals(productSize)
                    && recieptDetail.getProductSweetLevel().equals(productSweetLevel)) {
                recieptDetail.setQty(recieptDetail.getQty() + qty);
                recieptDetail.setTotalPrice(recieptDetail.getProductPrice() * recieptDetail.getQty());
                found = true;
                reciept.calculateTotal();
                break;
            }
        }

        if (!found) {
            reciept.addRecieptDetail(product, productSize, productSweetLevel, qty);
        }
        refreshReciept();
    }

    @Override
    public void buy(ProductModel product, int qty) {
        boolean found = false;
        for (RecieptDetailModel recieptDetail : reciept.getRecieptDetails()) {
//            System.out.println("------------------");
//            System.out.println("+ " +recieptDetail.getProductName());
//            System.out.println(recieptDetail.getQty());
//            System.out.println("------------------");

            if (recieptDetail.getProductName().equals(product.getName())) {
                recieptDetail.setQty(recieptDetail.getQty() + qty);
                recieptDetail.setTotalPrice(recieptDetail.getProductPrice() * recieptDetail.getQty());
                found = true;
                reciept.calculateTotal();
                break;
            }
        }

        if (!found) {
            reciept.addRecieptDetail(product, qty);
        }
        refreshReciept();
    }
}

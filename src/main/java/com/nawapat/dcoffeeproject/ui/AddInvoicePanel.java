/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.model.HistoryInvoiceDetailModel;
import com.nawapat.dcoffeeproject.model.HistoryInvoiceModel;
import com.nawapat.dcoffeeproject.model.MaterialModel;
import com.nawapat.dcoffeeproject.service.HistoryInvoiceService;
import com.nawapat.dcoffeeproject.service.MaterialService;
import com.nawapat.dcoffeeproject.service.UserService;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Asus
 */
public class AddInvoicePanel extends javax.swing.JPanel {

    private final JScrollPane scrMain;

    private MaterialService matService;
    private List<MaterialModel> matList;
    HistoryInvoiceService hisInvoiceService = new HistoryInvoiceService();
    private HistoryInvoiceModel hisInvoice;
    private ArrayList<HistoryInvoiceDetailModel> hisInvoiceDetails = new ArrayList<HistoryInvoiceDetailModel>();

    /**
     * Creates new form AddStock
     */
    public AddInvoicePanel(JScrollPane scrMain) {

        initComponents();
        this.scrMain = scrMain;

        showDate();
        showTime();

        initImage();

        hisInvoice = new HistoryInvoiceModel();
        hisInvoice.setUser(UserService.getCurrentUser());
        initInvoiceTable();
        tbInvoice.setRowHeight(30);
        tbInvoice.setModel(new AbstractTableModel() {
            String[] hearderHis = {"Key", "Name", "Company", "Price", "Amount", "Total"};

            @Override
            public int getRowCount() {
                return hisInvoice.getHisInvoiceDetail().size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public String getColumnName(int column) {
                return hearderHis[column];
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<HistoryInvoiceDetailModel> hisInvoiceDetails = hisInvoice.getHisInvoiceDetail();
                HistoryInvoiceDetailModel hisInvoiceDetail = hisInvoiceDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisInvoiceDetail.getMaterialKey();
                    case 1:
                        return hisInvoiceDetail.getMaterialName();
                    case 2:
                        return hisInvoiceDetail.getMaterialCompany();
                    case 3:
                        return hisInvoiceDetail.getMaterialPrice();
                    case 4:
                        return hisInvoiceDetail.getMaterialQty();
                    case 5:
                        return hisInvoiceDetail.getMaterialTotal();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<HistoryInvoiceDetailModel> hisInvoiceDetails = hisInvoice.getHisInvoiceDetail();
                HistoryInvoiceDetailModel hisInvoiceDetail = hisInvoiceDetails.get(rowIndex);
                if (columnIndex == 4) {
                    int amount = Integer.parseInt((String) aValue);
                    if (amount < 1) {
                        return;
                    }
                    hisInvoiceDetail.setMaterialQty(amount);
                    hisInvoice.calculateTotal();
                    refresHis();
                }
                if (columnIndex == 2) {
                    String company = (String) aValue;
                    hisInvoiceDetail.setMaterialCompany(company);
                    refresHis();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    case 4:
                        return true;
                    default:
                        return false;
                }
            }
        }
        );
    }

    private void initImage() {
        ImageIcon icon = new ImageIcon("./leftarrow.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((15 * width) / height), 15, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        btnBack.setIcon(icon);
        
        ImageIcon iconDel = new ImageIcon("./IconDel.png");
        Image imageDel = iconDel.getImage();
        Image newImageDel = imageDel.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconDel.setImage(newImageDel);
        btnDel.setIcon(iconDel);
        
        ImageIcon iconClear = new ImageIcon("./IconClear.png");
        Image imageClear = iconClear.getImage();
        Image newImageClear = imageClear.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconClear.setImage(newImageClear);
        btnClear.setIcon(iconClear);
        
        ImageIcon iconSave = new ImageIcon("./IconSave.png");
        Image imageSave = iconSave.getImage();
        Image newImageSave = imageSave.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconSave.setImage(newImageSave);
        btnSave.setIcon(iconSave);
    }

    private void initInvoiceTable() {
        matService = new MaterialService();
        matList = matService.getMaterials();
        tbHisInvoice.setRowHeight(30);
        tbHisInvoice.setModel(new AbstractTableModel() {
            String[] hearderHis = {"Key", "Name", "Unit"};

            @Override
            public String getColumnName(int column) {
                return hearderHis[column];
            }

            @Override
            public int getRowCount() {
                return matList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                MaterialModel mat = matList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return mat.getKey();
                    case 1:
                        return mat.getName();
                    case 2:
                        return mat.getUnit();
                    default:
                        return "Unknown";
                }
            }

        });

        tbHisInvoice.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tbHisInvoice.rowAtPoint(e.getPoint());
                int col = tbHisInvoice.columnAtPoint(e.getPoint());
                System.out.println(matList.get(row));
                MaterialModel matModel = matList.get(row);
                //HistoryInvoiceDetailModel hi = new HistoryInvoiceDetailModel(matModel.getId(), matModel.getKey(), matModel.getName(),
                //matModel.getUnit(), matModel.getPrice(), 1, (int) matModel.getPrice(), "", -1);
                hisInvoice.addInvoiceDetail(matModel, 1);
                tbInvoice.repaint();
                tbInvoice.revalidate();
            }

        });
    }

    private void refresHis() {
        tbInvoice.revalidate();
        tbInvoice.repaint();

    }

    public JScrollPane getScrMain() {
        return scrMain;
    }

    void showDate() { //โชว์Date
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date d = new java.util.Date();
        Date.setText(s.format(d));
    }

    void showTime() { //โชว์Time
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                java.util.Date d = new java.util.Date();
                Time.setText(s.format(d));
            }
        }).start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        Time = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbInvoice = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbHisInvoice = new javax.swing.JTable();

        setBackground(new java.awt.Color(238, 214, 196));

        jPanel1.setBackground(new java.awt.Color(238, 214, 196));
        jPanel1.setPreferredSize(new java.awt.Dimension(176, 69));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(72, 52, 52));
        jLabel1.setText("Add Invoice");

        Date.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Date.setForeground(new java.awt.Color(72, 52, 52));
        Date.setText("00/00/0000");

        Time.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Time.setForeground(new java.awt.Color(72, 52, 52));
        Time.setText("00.00.00 00");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Time)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Date)
                    .addComponent(Time))
                .addContainerGap())
        );

        tbInvoice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbInvoice.setForeground(new java.awt.Color(72, 52, 52));
        tbInvoice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Key", "Name", "Company", "Price", "Amount", "Total"
            }
        ));
        jScrollPane1.setViewportView(tbInvoice);

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBack.setForeground(new java.awt.Color(72, 52, 52));
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 243, 228));

        btnSave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(72, 52, 52));
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnClear.setForeground(new java.awt.Color(72, 52, 52));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnDel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnDel.setForeground(new java.awt.Color(72, 52, 52));
        btnDel.setText("Delete");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSave)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClear)
                    .addComponent(btnSave)
                    .addComponent(btnDel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tbHisInvoice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbHisInvoice.setForeground(new java.awt.Color(72, 52, 52));
        tbHisInvoice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Key", "Name", "Unit"
            }
        ));
        jScrollPane2.setViewportView(tbHisInvoice);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 979, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBack)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBack)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        scrMain.setViewportView(new HistoryInvoicePanel(scrMain));
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        System.out.println(" " + hisInvoice);
        hisInvoiceService.addNew(hisInvoice);
        scrMain.setViewportView(new HistoryInvoicePanel(scrMain));
        ImageIcon icon = new ImageIcon("./checked.png");
        Image image = icon.getImage(); // 
        Image newimg = image.getScaledInstance(45, 45, java.awt.Image.SCALE_SMOOTH); // 
        icon = new ImageIcon(newimg); // 
        JOptionPane.showMessageDialog(tbHisInvoice, "Add material invoice successfully!", "Material Invoice", HEIGHT, icon);

    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        ArrayList<HistoryInvoiceDetailModel> hisInvoiceDetails = hisInvoice.getHisInvoiceDetail();
        int input = JOptionPane.showConfirmDialog(this,
                "Do you want to clear ?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        if (input == 0) {
            hisInvoiceDetails.clear();

        }
        refreshHisInvoice();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        int selectedDel = tbInvoice.getSelectedRow();
        ArrayList<HistoryInvoiceDetailModel> hisInvoiceDetails = hisInvoice.getHisInvoiceDetail();
        HistoryInvoiceDetailModel hisInvoiceDetail = hisInvoiceDetails.get(selectedDel);
        if (selectedDel >= 0) {
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete " + hisInvoiceDetail.getMaterialName() + "?", "Caution!!", JOptionPane.YES_NO_OPTION,
                    JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                hisInvoice.delInvoiceDetail(hisInvoiceDetail);
                refreshHisInvoice();
                ImageIcon icon = new ImageIcon("./deleted.png");
                Image image = icon.getImage(); // 
                Image newimg = image.getScaledInstance(45, 45, java.awt.Image.SCALE_SMOOTH); // 
                icon = new ImageIcon(newimg); // 
                JOptionPane.showMessageDialog(tbHisInvoice, "Deleted successfully!", "Material Management", HEIGHT, icon);
            }
        }
    }//GEN-LAST:event_btnDelActionPerformed

    public void delInvoiceDetail(HistoryInvoiceDetailModel hisInvoiceDetail) {
        hisInvoiceDetails.remove(hisInvoiceDetail);
    }

    private void refreshHisInvoice() {
        tbInvoice.revalidate();
        tbInvoice.repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbHisInvoice;
    private javax.swing.JTable tbInvoice;
    // End of variables declaration//GEN-END:variables
}

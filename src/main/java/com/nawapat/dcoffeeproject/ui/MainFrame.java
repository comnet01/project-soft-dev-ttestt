/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.model.UserModel;
import com.nawapat.dcoffeeproject.service.UserService;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 *
 * @author asus
 */
public class MainFrame extends javax.swing.JFrame {

    private UserModel user;

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        txtUsername.setText(UserService.getCurrentUser().getEmployee().getName());
        int position = UserService.getCurrentUser().getEmployee().getPosition();
        switch (position) {
            case 0:
                txtPosition.setText("Manager");
                break;
            case 1:
                txtPosition.setText("Employee");
                break;
            default:
                // กรณีอื่นๆที่คุณต้องการจัดการ
                txtPosition.setText("Position not defined");
                break;
        }

        if (UserService.getCurrentUser().getEmployee().getPosition() > 0) {
            jbtnUser.setVisible(false);
            jbtnEmployee.setVisible(false);
            jbtnOrder.setVisible(false);
        }

        if (UserService.getCurrentUser().getEmployee().getPosition() >= 0) {
            scrMain.setViewportView(new ReportsPanel(scrMain));

        }

        ImageIcon icon = new ImageIcon("./logo1.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((45 * width) / height), 45, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);

        ImageIcon iconlogout = new ImageIcon("./logout.png");
        Image imagelogout = iconlogout.getImage();
        Image newImagelogout = imagelogout.getScaledInstance((int) ((20 * width) / height), 20, Image.SCALE_SMOOTH);
        iconlogout.setImage(newImagelogout);
        btnLogout.setIcon(iconlogout);

        ImageIcon iconPos = new ImageIcon("./pos.png");
        Image imagePos = iconPos.getImage();
        Image newImagePos = imagePos.getScaledInstance((int) ((30 * width) / height), 30, Image.SCALE_SMOOTH);
        iconPos.setImage(newImagePos);
        jbtnPos.setIcon(iconPos);

        ImageIcon iconPD = new ImageIcon("./iconproduct.png");
        Image imagePD = iconPD.getImage();
        Image newImagePD = imagePD.getScaledInstance((int) ((30 * width) / height), 30, Image.SCALE_SMOOTH);
        iconPD.setImage(newImagePD);
        jbtnProduct.setIcon(iconPD);

        ImageIcon iconMat = new ImageIcon("./material.png");
        Image imageMat = iconMat.getImage();
        Image newImageMat = imageMat.getScaledInstance((int) ((30 * width) / height), 30, Image.SCALE_SMOOTH);
        iconMat.setImage(newImageMat);
        jbtnMaterial.setIcon(iconMat);

        ImageIcon iconMember = new ImageIcon("./member.png");
        Image imageMember = iconMember.getImage();
        Image newImageMember = imageMember.getScaledInstance((int) ((30 * width) / height), 30, Image.SCALE_SMOOTH);
        iconMember.setImage(newImageMember);
        jbtnMember.setIcon(iconMember);

        ImageIcon iconSalary = new ImageIcon("./salary.png");
        Image imageSalary = iconSalary.getImage();
        Image newImageSalary = imageSalary.getScaledInstance((int) ((30 * width) / height), 30, Image.SCALE_SMOOTH);
        iconSalary.setImage(newImageSalary);
        jbtnSalary.setIcon(iconSalary);

        ImageIcon iconCheckin = new ImageIcon("./checkin.png");
        Image imageCheckin = iconCheckin.getImage();
        Image newImageCheckin = imageCheckin.getScaledInstance((int) ((30 * width) / height), 30, Image.SCALE_SMOOTH);
        iconCheckin.setImage(newImageCheckin);
        btnCheckin.setIcon(iconCheckin);

        ImageIcon iconEmployee = new ImageIcon("./employee.png");
        Image imageEmployee = iconEmployee.getImage();
        Image newImageEmployee = imageEmployee.getScaledInstance((int) ((30 * width) / height), 30, Image.SCALE_SMOOTH);
        iconEmployee.setImage(newImageEmployee);
        jbtnEmployee.setIcon(iconEmployee);

        ImageIcon iconUser = new ImageIcon("./user.png");
        Image imageUser = iconUser.getImage();
        Image newImageUser = imageUser.getScaledInstance((int) ((30 * width) / height), 30, Image.SCALE_SMOOTH);
        iconUser.setImage(newImageUser);
        jbtnUser.setIcon(iconUser);

        ImageIcon iconOrder = new ImageIcon("./reciept.png");
        Image imageOrder = iconOrder.getImage();
        Image newImageOrder = imageOrder.getScaledInstance((int) ((30 * width) / height), 30, Image.SCALE_SMOOTH);
        iconOrder.setImage(newImageOrder);
        jbtnOrder.setIcon(iconOrder);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelHead = new javax.swing.JPanel();
        txtUsername = new javax.swing.JLabel();
        lblImage = new javax.swing.JLabel();
        txtPosition = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        panelNavigation = new javax.swing.JPanel();
        jbtnPos = new javax.swing.JButton();
        jbtnProduct = new javax.swing.JButton();
        jbtnMaterial = new javax.swing.JButton();
        jbtnMember = new javax.swing.JButton();
        jbtnEmployee = new javax.swing.JButton();
        jbtnSalary = new javax.swing.JButton();
        jbtnUser = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        btnCheckin = new javax.swing.JButton();
        jbtnOrder = new javax.swing.JButton();
        scrMain = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelHead.setBackground(new java.awt.Color(72, 52, 52));

        txtUsername.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        txtUsername.setForeground(new java.awt.Color(229, 229, 203));
        txtUsername.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txtUsername.setText("Name");

        lblImage.setPreferredSize(new java.awt.Dimension(38, 38));
        lblImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblImageMouseClicked(evt);
            }
        });

        txtPosition.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        txtPosition.setForeground(new java.awt.Color(229, 229, 203));
        txtPosition.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txtPosition.setText("Position");

        jLabel1.setFont(new java.awt.Font("Cooper Black", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(229, 229, 203));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Name :");

        jLabel2.setFont(new java.awt.Font("Cooper Black", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(229, 229, 203));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("Position :");

        javax.swing.GroupLayout panelHeadLayout = new javax.swing.GroupLayout(panelHead);
        panelHead.setLayout(panelHeadLayout);
        panelHeadLayout.setHorizontalGroup(
            panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeadLayout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 933, Short.MAX_VALUE)
                .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPosition, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        panelHeadLayout.setVerticalGroup(
            panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelHeadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblImage, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelHeadLayout.createSequentialGroup()
                        .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUsername)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPosition)
                            .addComponent(jLabel2))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        panelNavigation.setBackground(new java.awt.Color(107, 79, 79));

        jbtnPos.setBackground(new java.awt.Color(183, 183, 162));
        jbtnPos.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        jbtnPos.setForeground(new java.awt.Color(72, 52, 52));
        jbtnPos.setText("POS");
        jbtnPos.setToolTipText("");
        jbtnPos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jbtnPos.setPreferredSize(new java.awt.Dimension(81, 45));
        jbtnPos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnPosActionPerformed(evt);
            }
        });

        jbtnProduct.setBackground(new java.awt.Color(183, 183, 162));
        jbtnProduct.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        jbtnProduct.setForeground(new java.awt.Color(72, 52, 52));
        jbtnProduct.setText("Product");
        jbtnProduct.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jbtnProduct.setPreferredSize(new java.awt.Dimension(81, 45));
        jbtnProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnProductActionPerformed(evt);
            }
        });

        jbtnMaterial.setBackground(new java.awt.Color(183, 183, 162));
        jbtnMaterial.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        jbtnMaterial.setForeground(new java.awt.Color(72, 52, 52));
        jbtnMaterial.setText("Material");
        jbtnMaterial.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jbtnMaterial.setPreferredSize(new java.awt.Dimension(81, 45));
        jbtnMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnMaterialActionPerformed(evt);
            }
        });

        jbtnMember.setBackground(new java.awt.Color(183, 183, 162));
        jbtnMember.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        jbtnMember.setForeground(new java.awt.Color(72, 52, 52));
        jbtnMember.setText("Member");
        jbtnMember.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jbtnMember.setPreferredSize(new java.awt.Dimension(81, 45));
        jbtnMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnMemberActionPerformed(evt);
            }
        });

        jbtnEmployee.setBackground(new java.awt.Color(183, 183, 162));
        jbtnEmployee.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        jbtnEmployee.setForeground(new java.awt.Color(72, 52, 52));
        jbtnEmployee.setText("Employee");
        jbtnEmployee.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jbtnEmployee.setPreferredSize(new java.awt.Dimension(81, 45));
        jbtnEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnEmployeeActionPerformed(evt);
            }
        });

        jbtnSalary.setBackground(new java.awt.Color(183, 183, 162));
        jbtnSalary.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        jbtnSalary.setForeground(new java.awt.Color(72, 52, 52));
        jbtnSalary.setText("Salary");
        jbtnSalary.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jbtnSalary.setPreferredSize(new java.awt.Dimension(81, 45));
        jbtnSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnSalaryActionPerformed(evt);
            }
        });

        jbtnUser.setBackground(new java.awt.Color(183, 183, 162));
        jbtnUser.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        jbtnUser.setForeground(new java.awt.Color(72, 52, 52));
        jbtnUser.setText("User");
        jbtnUser.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jbtnUser.setPreferredSize(new java.awt.Dimension(81, 45));
        jbtnUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnUserActionPerformed(evt);
            }
        });

        btnLogout.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        btnLogout.setForeground(new java.awt.Color(72, 52, 52));
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        btnCheckin.setBackground(new java.awt.Color(183, 183, 162));
        btnCheckin.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        btnCheckin.setForeground(new java.awt.Color(72, 52, 52));
        btnCheckin.setText("Check in");
        btnCheckin.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnCheckin.setPreferredSize(new java.awt.Dimension(81, 45));
        btnCheckin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckinActionPerformed(evt);
            }
        });

        jbtnOrder.setBackground(new java.awt.Color(183, 183, 162));
        jbtnOrder.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        jbtnOrder.setForeground(new java.awt.Color(72, 52, 52));
        jbtnOrder.setText("Order");
        jbtnOrder.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jbtnOrder.setPreferredSize(new java.awt.Dimension(81, 45));
        jbtnOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnOrderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelNavigationLayout = new javax.swing.GroupLayout(panelNavigation);
        panelNavigation.setLayout(panelNavigationLayout);
        panelNavigationLayout.setHorizontalGroup(
            panelNavigationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNavigationLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(panelNavigationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnCheckin, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(jbtnSalary, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(jbtnEmployee, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(jbtnMember, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(jbtnMaterial, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(jbtnProduct, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(jbtnPos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(jbtnUser, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jbtnOrder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnLogout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 31, Short.MAX_VALUE))
        );
        panelNavigationLayout.setVerticalGroup(
            panelNavigationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNavigationLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jbtnPos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jbtnProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jbtnMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jbtnMember, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jbtnSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCheckin, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jbtnEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jbtnUser, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jbtnOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17))
        );

        scrMain.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(panelHead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelNavigation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(scrMain)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelHead, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelNavigation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scrMain))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbtnPosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnPosActionPerformed
        scrMain.setViewportView(new POSPanel());
    }//GEN-LAST:event_jbtnPosActionPerformed

    private void jbtnMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnMaterialActionPerformed
        scrMain.setViewportView(new MaterialPanel(scrMain));
    }//GEN-LAST:event_jbtnMaterialActionPerformed

    private void jbtnMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnMemberActionPerformed
        scrMain.setViewportView(new CustomerPanel());
    }//GEN-LAST:event_jbtnMemberActionPerformed

    private void jbtnProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnProductActionPerformed
        scrMain.setViewportView(new ProductPanel());
    }//GEN-LAST:event_jbtnProductActionPerformed

    private void jbtnEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnEmployeeActionPerformed
        scrMain.setViewportView(new EmployeePanel());
    }//GEN-LAST:event_jbtnEmployeeActionPerformed

    private void jbtnSalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnSalaryActionPerformed
        scrMain.setViewportView(new SalaryPanel());
    }//GEN-LAST:event_jbtnSalaryActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        int confirmed = JOptionPane.showConfirmDialog(null,
                "Do you want to logout?", "Are you sure?",
                JOptionPane.YES_NO_OPTION);

        if (confirmed == JOptionPane.YES_OPTION) {
            this.dispose();
            new LoginFrame().setVisible(true); // สร้างหน้า LoginFrom และแสดง
        }
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void jbtnUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnUserActionPerformed
        scrMain.setViewportView(new UserPanel());
    }//GEN-LAST:event_jbtnUserActionPerformed

    private void btnCheckinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckinActionPerformed
        scrMain.setViewportView(new CheckInCheckOutJPanel());
    }//GEN-LAST:event_btnCheckinActionPerformed

    private void jbtnOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnOrderActionPerformed
        scrMain.setViewportView(new OrderHistoryPanel(scrMain));
    }//GEN-LAST:event_jbtnOrderActionPerformed

    private void lblImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblImageMouseClicked
        scrMain.setViewportView(new ReportsPanel(scrMain));
    }//GEN-LAST:event_lblImageMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheckin;
    private javax.swing.JButton btnLogout;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton jbtnEmployee;
    private javax.swing.JButton jbtnMaterial;
    private javax.swing.JButton jbtnMember;
    private javax.swing.JButton jbtnOrder;
    private javax.swing.JButton jbtnPos;
    private javax.swing.JButton jbtnProduct;
    private javax.swing.JButton jbtnSalary;
    private javax.swing.JButton jbtnUser;
    private javax.swing.JLabel lblImage;
    private javax.swing.JPanel panelHead;
    private javax.swing.JPanel panelNavigation;
    private javax.swing.JScrollPane scrMain;
    private javax.swing.JLabel txtPosition;
    private javax.swing.JLabel txtUsername;
    // End of variables declaration//GEN-END:variables

}

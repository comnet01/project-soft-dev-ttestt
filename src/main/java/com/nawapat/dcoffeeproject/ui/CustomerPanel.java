/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.model.CustomerModel;
import com.nawapat.dcoffeeproject.model.MaterialModel;
import com.nawapat.dcoffeeproject.service.CustomerService;
import com.nawapat.dcoffeeproject.service.MaterialService;
import component.InputDialogMember;
import component.InputDialogProduct;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author kornn
 */
public class CustomerPanel extends javax.swing.JPanel {

    private final CustomerService customerService;
    private List<CustomerModel> list;
    private CustomerModel editedCus;
    private CustomerModel Customer;

    /**
     * Creates new form CustomerPanel
     */
    public CustomerPanel() {
        initComponents();
        initImage();

        customerService = new CustomerService();
        list = customerService.getCustomers();
//        enableForm(false);
        tblCus.setRowHeight(30);
        tblCus.setModel(new AbstractTableModel() {
            String[] columnNames = {"Key", "Name", "Phone", "Point"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CustomerModel customer = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return customer.getKey();
                    case 1:
                        return customer.getName();
                    case 2:
                        return customer.getPhone();
                    case 3:
                        return customer.getPoint();
                    default:
                        return "Unknown";
                }
            }
        });
        showDate(); //โชว์Date
        showTime(); //โชว์Time
    }

    private void initImage() {
        ImageIcon icon = new ImageIcon("./member.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((57 * width) / height), 57, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);

        ImageIcon iconEdit = new ImageIcon("./IconEdit.png");
        Image imageEdit = iconEdit.getImage();
        Image newImageEdit = imageEdit.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconEdit.setImage(newImageEdit);
        btnEdit.setIcon(iconEdit);

        ImageIcon iconDel = new ImageIcon("./IconDel.png");
        Image imageDel = iconDel.getImage();
        Image newImageDel = imageDel.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconDel.setImage(newImageDel);
        btnDelete.setIcon(iconDel);

//        ImageIcon iconClear = new ImageIcon("./IconClear.png");
//        Image imageClear = iconClear.getImage();
//        Image newImageClear = imageClear.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
//        iconClear.setImage(newImageClear);
//        btnClear.setIcon(iconClear);
//        ImageIcon iconSave = new ImageIcon("./IconSave.png");
//        Image imageSave = iconSave.getImage();
//        Image newImageSave = imageSave.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
//        iconSave.setImage(newImageSave);
//        btnSave.setIcon(iconSave);
    }

    void showDate() { //โชว์Date
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date d = new java.util.Date();
        Date.setText(s.format(d));
    }

    void showTime() { //โชว์Time
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                java.util.Date d = new java.util.Date();
                Time.setText(s.format(d));
            }
        }).start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCus = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        Time = new javax.swing.JLabel();
        lblImage = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(238, 214, 196));

        tblCus.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblCus.setForeground(new java.awt.Color(72, 52, 52));
        tblCus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblCus);

        jPanel2.setBackground(new java.awt.Color(255, 243, 228));

        btnEdit.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnEdit.setForeground(new java.awt.Color(72, 52, 52));
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(72, 52, 52));
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete)
                .addContainerGap(574, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEdit)
                    .addComponent(btnDelete))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(238, 214, 196));
        jPanel3.setPreferredSize(new java.awt.Dimension(89, 69));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(72, 52, 52));
        jLabel1.setText("Customer Management");

        Date.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Date.setForeground(new java.awt.Color(72, 52, 52));
        Date.setText("00/00/0000");

        Time.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Time.setForeground(new java.awt.Color(72, 52, 52));
        Time.setText("00.00.00 00");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Time))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Date)
                            .addComponent(Time))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 730, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 718, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedIndex = tblCus.getSelectedRow();
        if (selectedIndex >= 0) {
            editedCus = list.get(selectedIndex);
//            setObjectToForm();
//            enableForm(true);
            openDialog1();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblCus.getSelectedRow();
        if (selectedIndex >= 0) {
            editedCus = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                customerService.delete(editedCus);
                refreshTable();
                ImageIcon icon = new ImageIcon("./deleted.png");
                Image image = icon.getImage(); // 
                Image newimg = image.getScaledInstance(45, 45, java.awt.Image.SCALE_SMOOTH); // 
                icon = new ImageIcon(newimg); // 
                JOptionPane.showMessageDialog(tblCus, "Deleted successfully!", "Customer Management", HEIGHT, icon);
            }
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void refreshTable() {
        list = customerService.getCustomers();
        tblCus.revalidate();
        tblCus.repaint();
    }

//    private void enableForm(boolean status) {
//        if (status == false) {
//            txtKey.setText("");
//            txtName.setText("");
//            txtPhone.setText("");
//            txtPoint.setText("");;
//        }
//        txtKey.setEnabled(status);
//        txtName.setEnabled(status);
//        btnSave.setEnabled(status);
//        btnClear.setEnabled(status);
//        txtPhone.setEnabled(status);
//        txtPoint.setEnabled(status);
//        txtKey.requestFocus();
//    }
//
//    private void setFormToObject() {
//        editedCus.setKey(txtKey.getText());
//        editedCus.setName(txtName.getText());
//        editedCus.setPhone(txtPhone.getText());
//        int point = Integer.parseInt(txtPoint.getText());
//        editedCus.setPoint(point);
//    }
//
//    private void setObjectToForm() {
//        txtKey.setText(editedCus.getKey());
//        txtName.setText(editedCus.getName());
//        txtPhone.setText(editedCus.getPhone());
//        txtPoint.setText(String.valueOf(editedCus.getPoint()));
//    }
    private void openDialog1() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        InputDialogMember inputDialog = new InputDialogMember(frame, editedCus);
        inputDialog.setLocationRelativeTo(this);
        inputDialog.setVisible(true);
        inputDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }

        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblImage;
    private javax.swing.JTable tblCus;
    // End of variables declaration//GEN-END:variables
}

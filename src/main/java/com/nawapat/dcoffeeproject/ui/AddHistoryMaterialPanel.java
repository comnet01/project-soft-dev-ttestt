/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.model.HistoryMatDetailModel;
import com.nawapat.dcoffeeproject.model.HistoryMatModel;
import com.nawapat.dcoffeeproject.model.MaterialModel;
import com.nawapat.dcoffeeproject.model.RecieptDetailModel;
import com.nawapat.dcoffeeproject.model.RecieptModel;
import com.nawapat.dcoffeeproject.service.HistoryMatService;
import com.nawapat.dcoffeeproject.service.MaterialService;
import com.nawapat.dcoffeeproject.service.UserService;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author asus
 */
public class AddHistoryMaterialPanel extends javax.swing.JPanel {

    private final JScrollPane scrMain;

    private HistoryMatDetailModel editedHisMatDetail;
    HistoryMatModel hismat;
    ArrayList<HistoryMatDetailModel> getHisMatDetail;
    private MaterialService materialService;
    private List<MaterialModel> list;
    private MaterialModel editedMat;
    HistoryMatService hismatService = new HistoryMatService();
    private ArrayList<HistoryMatDetailModel> hismatDetails = new ArrayList<HistoryMatDetailModel>();

    public AddHistoryMaterialPanel(JScrollPane scrMain) {
        initComponents();
        this.scrMain = scrMain;
        showDate(); //โชว์Date
        showTime(); //โชว์Time
        initImage();

        hismat = new HistoryMatModel(); // ตั้งค่า hisMat ใหม่
        hismat.setUser(UserService.getCurrentUser());

        initMat();
        tbHisMat.setRowHeight(30);
        tbHisMat.setModel(new AbstractTableModel() {
            String[] columnNames = {"Key", "Name", "Remain"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return hismat.getHisMatDetail().size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<HistoryMatDetailModel> hisMatDetails = hismat.getHisMatDetail();
                HistoryMatDetailModel hisMatDetail = hisMatDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisMatDetail.getMaterialKey();
                    case 1:
                        return hisMatDetail.getMaterialName();
                    case 2:
                        return hisMatDetail.getMaterialRemain();
                    default:
                        return " ";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<HistoryMatDetailModel> hisMatDetails = hismat.getHisMatDetail();
                HistoryMatDetailModel hisMatDetail = hisMatDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int remain = Integer.parseInt((String) aValue);
                    if (remain < 1) {
                        return;
                    }
                    hisMatDetail.setMaterialRemain(remain);
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        });
    }

    private void initImage() {
        ImageIcon icon = new ImageIcon("./leftarrow.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((15 * width) / height), 15, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        btnBack.setIcon(icon);
        
        ImageIcon iconDel = new ImageIcon("./IconDel.png");
        Image imageDel = iconDel.getImage();
        Image newImageDel = imageDel.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconDel.setImage(newImageDel);
        btnDel.setIcon(iconDel);
        
        ImageIcon iconClear = new ImageIcon("./IconClear.png");
        Image imageClear = iconClear.getImage();
        Image newImageClear = imageClear.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconClear.setImage(newImageClear);
        btnClear.setIcon(iconClear);
        
        ImageIcon iconSave = new ImageIcon("./IconSave.png");
        Image imageSave = iconSave.getImage();
        Image newImageSave = imageSave.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconSave.setImage(newImageSave);
        btnSave.setIcon(iconSave);
    }

    private void initMat() {
        materialService = new MaterialService();
        list = materialService.getMaterials();
        tbMat.setRowHeight(30);
        tbMat.setModel(new AbstractTableModel() {
            String[] columnNames = {"Key", "Name", "Unit"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                MaterialModel stock = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return stock.getKey();
                    case 1:
                        return stock.getName();
                    case 2:
                        return stock.getUnit();
                    default:
                        return "Unknown";
                }
            }
        });
        tbMat.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tbMat.rowAtPoint(e.getPoint());
                int col = tbMat.columnAtPoint(e.getPoint());
                System.out.println(list.get(row));
                MaterialModel materialModel = list.get(row);
                HistoryMatDetailModel hd = new HistoryMatDetailModel(materialModel.getId(), materialModel.getKey(), materialModel.getName(), materialModel.getUnit(), 0, -1);
                hismat.addHisMatDetail(hd);
                tbHisMat.repaint();
                tbHisMat.revalidate();

            }

        });

    }

    void showDate() { //โชว์Date
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date d = new java.util.Date();
        Date.setText(s.format(d));
    }

    void showTime() { //โชว์Time
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                java.util.Date d = new java.util.Date();
                Time.setText(s.format(d));
            }
        }).start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblMat = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        Time = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btnClear = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbMat = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbHisMat = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(238, 214, 196));
        jPanel1.setForeground(new java.awt.Color(72, 52, 52));

        jPanel2.setBackground(new java.awt.Color(238, 214, 196));
        jPanel2.setPreferredSize(new java.awt.Dimension(203, 69));

        lblMat.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblMat.setForeground(new java.awt.Color(72, 52, 52));
        lblMat.setText("Add History Material");

        Date.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Date.setForeground(new java.awt.Color(72, 52, 52));
        Date.setText("00/00/0000");

        Time.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Time.setForeground(new java.awt.Color(72, 52, 52));
        Time.setText("00.00.00 00");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMat, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Time)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMat)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Date)
                    .addComponent(Time))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 243, 228));
        jPanel3.setPreferredSize(new java.awt.Dimension(240, 35));

        btnClear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnClear.setForeground(new java.awt.Color(72, 52, 52));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(72, 52, 52));
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnDel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnDel.setForeground(new java.awt.Color(72, 52, 52));
        btnDel.setText("Delete");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(616, Short.MAX_VALUE)
                .addComponent(btnDel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSave)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClear)
                    .addComponent(btnSave)
                    .addComponent(btnDel))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(238, 214, 196));

        tbMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbMat.setForeground(new java.awt.Color(72, 52, 52));
        tbMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Key", "Name", "Unit"
            }
        ));
        jScrollPane1.setViewportView(tbMat);

        tbHisMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbHisMat.setForeground(new java.awt.Color(72, 52, 52));
        tbHisMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Key", "Name", "Remain"
            }
        ));
        jScrollPane2.setViewportView(tbHisMat);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)))
        );

        jPanel5.setBackground(new java.awt.Color(238, 214, 196));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBack.setForeground(new java.awt.Color(72, 52, 52));
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 847, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 847, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(82, 82, 82)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 847, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 735, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        ArrayList<HistoryMatDetailModel> hismatDetails = hismat.getHisMatDetail();
        int input = JOptionPane.showConfirmDialog(this,
                "Do you want to Clear ?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        if (input == 0) {
            hismatDetails.clear();

        }
        refreshHisMat();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        System.out.println(" " + hismat);
        hismatService.addNew(hismat);
        scrMain.setViewportView(new HistoryMat(scrMain));
        ImageIcon icon = new ImageIcon("./checked.png");
        Image image = icon.getImage(); // 
        Image newimg = image.getScaledInstance(45, 45, java.awt.Image.SCALE_SMOOTH); // 
        icon = new ImageIcon(newimg); // 
        JOptionPane.showMessageDialog(tbHisMat, "Add material history successfully!", "Material History", HEIGHT, icon);

    }//GEN-LAST:event_btnSaveActionPerformed

//    private void clearHisMat() {
//        hismat = new HistoryMatModel();
//        hismat.setUser(UserService.getCurrentUser());
//        refreshReciept();
//    }
    private void refreshHisMat() {
        tbHisMat.revalidate();
        tbHisMat.repaint();
    }


    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        scrMain.setViewportView(new HistoryMat(scrMain));
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        // TODO add your handling code here:
        int selectedDel = tbHisMat.getSelectedRow();
        ArrayList<HistoryMatDetailModel> hismatDetails = hismat.getHisMatDetail();
        HistoryMatDetailModel hismatDetail = hismatDetails.get(selectedDel);
        if (selectedDel >= 0) {
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete " + hismatDetail.getMaterialName() + "?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                hismat.delHisMatDetail(hismatDetail);
                refreshHisMat();
                ImageIcon icon = new ImageIcon("./deleted.png");
                Image image = icon.getImage(); // 
                Image newimg = image.getScaledInstance(45, 45, java.awt.Image.SCALE_SMOOTH); // 
                icon = new ImageIcon(newimg); // 
                JOptionPane.showMessageDialog(tbMat, "Deleted successfully!", "Material Management", HEIGHT, icon);
            }
          
        }
    }//GEN-LAST:event_btnDelActionPerformed

    public void delhismatDetail(HistoryMatDetailModel hismatDetail) {
        hismatDetails.remove(hismatDetail);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnSave;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblMat;
    private javax.swing.JTable tbHisMat;
    private javax.swing.JTable tbMat;
    // End of variables declaration//GEN-END:variables
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.model.MonthReport;
import com.nawapat.dcoffeeproject.model.RemainsReport;
import com.nawapat.dcoffeeproject.model.SalesReport;
import com.nawapat.dcoffeeproject.service.MonthReportSevice;
import com.nawapat.dcoffeeproject.service.RemainsSevice;
import com.nawapat.dcoffeeproject.service.SalesSevice;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author parametsmac
 */
public class ReportsPanel extends javax.swing.JPanel {

    private final SalesSevice salesService;
    private final List<SalesReport> salesList;
    private AbstractTableModel model;
    private DefaultPieDataset pieDataset;
    private final RemainsSevice remainsService;
    private final List<RemainsReport> remainsList;
    private AbstractTableModel remainsmodel;
    private DefaultCategoryDataset barDataset;
    private final List<SalesReport> salesList1;
    private final JScrollPane scrMain;
    private AbstractTableModel model1;
    private AbstractTableModel monthmodel;
    private final MonthReportSevice monthRepService;
    private final List<MonthReport> monthList;

    /**
     * Creates new form ReportsPanel
     */
    public ReportsPanel(JScrollPane scrMain) {
        initComponents();
        this.scrMain = scrMain;
        salesService = new SalesSevice();
        salesList = salesService.getTopTenSalesByTotalPrice();
        salesList1 = salesService.getTopTenSalesByTotalPriceMonth();
        remainsService = new RemainsSevice();
        remainsList = remainsService.getTopTenRemainsByTotalRemains();
        monthRepService = new MonthReportSevice();
        monthList = monthRepService.getAllMonthReport();
        showDate();
        showTime();
        initTable();
        initTableMonth();
        initTableMonthReport();
        initTable1();
        initPieChart();
        loadPieDataset();
        initBarChart();
        loadBarDataset();

        ImageIcon icon = new ImageIcon("./dashboard.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((57 * width) / height), 57, Image.SCALE_SMOOTH);

        icon.setImage(newImage);

        lblhome.setIcon(icon);

        ImageIcon icon1 = new ImageIcon("./clock.png");
        Image image1 = icon1.getImage();
        Image newImage1 = image1.getScaledInstance((int) ((20 * width) / height), 20, Image.SCALE_SMOOTH);
        icon1.setImage(newImage1);
        btnM.setIcon(icon1);

        ImageIcon icon2 = new ImageIcon("./calendar.png");
        Image image2 = icon2.getImage();
        Image newImage2 = image2.getScaledInstance((int) ((20 * width) / height), 20, Image.SCALE_SMOOTH);
        icon2.setImage(newImage2);
        btnM1.setIcon(icon2);

        ImageIcon icon3 = new ImageIcon("./bar-graph.png");
        Image image3 = icon3.getImage();
        Image newImage3 = image3.getScaledInstance((int) ((45 * width) / height), 45, Image.SCALE_SMOOTH);
        icon3.setImage(newImage3);
        btnMbar.setIcon(icon3);

        ImageIcon icon4 = new ImageIcon("./pie.png");
        Image image4 = icon4.getImage();
        Image newImage4 = image4.getScaledInstance((int) ((45 * width) / height), 45, Image.SCALE_SMOOTH);
        icon4.setImage(newImage4);
        btnMpie.setIcon(icon4);

        ImageIcon icon5 = new ImageIcon("./expanse.png");
        Image image5 = icon5.getImage();
        Image newImage5 = image5.getScaledInstance((int) ((15 * width) / height), 15, Image.SCALE_SMOOTH);
        icon5.setImage(newImage5);
        btnExpanse.setIcon(icon5);

    }

    private void initPieChart() {
        pieDataset = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart(
                "Top 10 Product Sales All Time", // chart title
                pieDataset, // data
                true, // include legend
                true,
                false);
        ChartPanel chartPanel = new ChartPanel(chart);
        pnlSales.add(chartPanel);
    }

    private void loadPieDataset() {
        pieDataset.clear();
        for (SalesReport a : salesList) {
            pieDataset.setValue(a.getName(), a.getTotalPrice());
        }
    }

    private void initPieChart1() {
        pieDataset = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart(
                "November Top 10 Product Sales", // chart title
                pieDataset, // data
                true, // include legend
                true,
                false);
        ChartPanel chartPanel = new ChartPanel(chart);
        pnlSales.add(chartPanel);
    }

    private void loadPieDataset1() {
        pieDataset.clear();
        for (SalesReport a : salesList1) {
            pieDataset.setValue(a.getName(), a.getTotalPrice());
        }
    }

    private void initPieChartM() {
        pieDataset = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart(
                "Month Income Report", // chart title
                pieDataset, // data
                true, // include legend
                true,
                false);
        ChartPanel chartPanel = new ChartPanel(chart);
        pnlMMM.add(chartPanel);
    }

    private void loadPieDatasetM() {
        pieDataset.clear();
        for (MonthReport a : monthList) {
            pieDataset.setValue("January", a.getTotal_sales_januray());
            pieDataset.setValue("February", a.getTotal_sales_feb());
            pieDataset.setValue("March", a.getTotal_sales_march());
            pieDataset.setValue("April", a.getTotal_sales_april());
            pieDataset.setValue("May", a.getTotal_sales_may());
            pieDataset.setValue("June", a.getTotal_sales_june());
            pieDataset.setValue("July", a.getTotal_sales_july());
            pieDataset.setValue("August", a.getTotal_sales_august());
            pieDataset.setValue("September", a.getTotal_sales_september());
            pieDataset.setValue("October", a.getTotal_sales_october());
            pieDataset.setValue("November", a.getTotal_sales_november());
        }
    }

//    private void initBarChart() {
//        barDataset = new DefaultCategoryDataset();
//        JFreeChart chart = ChartFactory.createBarChart(
//                "Material Least Remain",
//                "Material",
//                "Remain",
//                barDataset,
//                PlotOrientation.VERTICAL,
//                true, true, false);
//        ChartPanel chartPanel = new ChartPanel(chart);
//        pnlRemains.add(chartPanel);
//    }
//
//    private void loadBarDataset() {
//        barDataset.clear();
//        for (RemainsReport a : remainsList) {
//            barDataset.setValue(a.getTotalRemain(), "Remain", a.getName());
//        }
//    }
    private void initBarChart() {
        barDataset = new DefaultCategoryDataset();
        JFreeChart chart = ChartFactory.createBarChart(
                "Month Income Report",
                "Month",
                "Income",
                barDataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        pnlMMM.add(chartPanel);
    }

    private void loadBarDataset() {
        barDataset.clear();
        for (MonthReport a : monthList) {
            barDataset.addValue(a.getTotal_sales_januray(), "January", "Income");
            barDataset.addValue(a.getTotal_sales_feb(), "February", "Income");
            barDataset.addValue(a.getTotal_sales_march(), "March", "Income");
            barDataset.addValue(a.getTotal_sales_april(), "April", "Income");
            barDataset.addValue(a.getTotal_sales_may(), "May", "Income");
            barDataset.addValue(a.getTotal_sales_june(), "June", "Income");
            barDataset.addValue(a.getTotal_sales_july(), "July", "Income");
            barDataset.addValue(a.getTotal_sales_august(), "August", "Income");
            barDataset.addValue(a.getTotal_sales_september(), "September", "Income");
            barDataset.addValue(a.getTotal_sales_october(), "October", "Income");
            barDataset.addValue(a.getTotal_sales_november(), "November", "Income");
        }
    }

    private void initTable() {
        tblSales.setRowHeight(30);
        model = new AbstractTableModel() {
            String[] colNames = {"Product Name", "Quantity", "Price"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return salesList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                SalesReport sales = salesList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return sales.getName();
                    case 1:
                        return sales.getTotalQuantity();
                    case 2:
                        return sales.getTotalPrice();
                    default:
                        return "";
                }
            }
        };
        tblSales.setModel(model);
    }

    private void initTableMonth() {
        tblSales.setRowHeight(30);
        model1 = new AbstractTableModel() {
            String[] colNames = {"Product Name", "Quantity", "Price"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return salesList1.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                SalesReport sales1 = salesList1.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return sales1.getName();
                    case 1:
                        return sales1.getTotalQuantity();
                    case 2:
                        return sales1.getTotalPrice();
                    default:
                        return "";
                }
            }
        };

    }

    private void initTable1() {
        tblRemains.setRowHeight(30);
        remainsmodel = new AbstractTableModel() {
            String[] colNames = {"Material ID", "Material Name", "Remain", "Unit"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return remainsList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                RemainsReport remains = remainsList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return remains.getId();
                    case 1:
                        return remains.getName();
                    case 2:
                        return remains.getTotalRemain();
                    case 3:
                        return remains.getUnit();
                    default:
                        return "";
                }
            }
        };
        tblRemains.setModel(remainsmodel);
    }

    private void initTableMonthReport() {
        monthmodel = new AbstractTableModel() {
            String[] colNames = {"Januray", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return monthList.size();
            }

            @Override
            public int getColumnCount() {
                return 12;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                MonthReport months = monthList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return months.getTotal_sales_januray();
                    case 1:
                        return months.getTotal_sales_feb();
                    case 2:
                        return months.getTotal_sales_march();
                    case 3:
                        return months.getTotal_sales_april();
                    case 4:
                        return months.getTotal_sales_may();
                    case 5:
                        return months.getTotal_sales_june();
                    case 6:
                        return months.getTotal_sales_july();
                    case 7:
                        return months.getTotal_sales_august();
                    case 8:
                        return months.getTotal_sales_september();
                    case 9:
                        return months.getTotal_sales_october();
                    case 10:
                        return months.getTotal_sales_november();
                    case 11:
                        return months.getTotal_sales_december();
                    default:
                        return "";
                }
            }
        };

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlSales = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSales = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblRemains = new javax.swing.JTable();
        txtSalename = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblhome = new javax.swing.JLabel();
        btnM = new javax.swing.JButton();
        btnM1 = new javax.swing.JButton();
        btnExpanse = new javax.swing.JButton();
        pnlMMM = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnMbar = new javax.swing.JButton();
        btnMpie = new javax.swing.JButton();
        Time = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 243, 228));

        pnlSales.setBackground(new java.awt.Color(107, 79, 79));
        pnlSales.setForeground(new java.awt.Color(72, 52, 52));

        jScrollPane1.setBackground(new java.awt.Color(255, 243, 228));

        tblSales.setForeground(new java.awt.Color(72, 52, 52));
        tblSales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblSales);

        jScrollPane2.setBackground(new java.awt.Color(255, 243, 228));

        tblRemains.setForeground(new java.awt.Color(72, 52, 52));
        tblRemains.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblRemains);

        txtSalename.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtSalename.setForeground(new java.awt.Color(72, 52, 52));
        txtSalename.setText("Top 10 Product Sales All Time :");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(72, 52, 52));
        jLabel3.setText("Material Least Remain :");

        jLabel4.setFont(new java.awt.Font("Helvetica Neue", 3, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(72, 52, 52));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("Dashboard");

        btnM.setBackground(new java.awt.Color(183, 183, 162));
        btnM.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        btnM.setForeground(new java.awt.Color(72, 52, 52));
        btnM.setToolTipText("");
        btnM.setPreferredSize(new java.awt.Dimension(81, 45));
        btnM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMActionPerformed(evt);
            }
        });

        btnM1.setBackground(new java.awt.Color(183, 183, 162));
        btnM1.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        btnM1.setForeground(new java.awt.Color(72, 52, 52));
        btnM1.setToolTipText("");
        btnM1.setPreferredSize(new java.awt.Dimension(81, 45));
        btnM1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnM1ActionPerformed(evt);
            }
        });

        btnExpanse.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnExpanse.setForeground(new java.awt.Color(72, 52, 52));
        btnExpanse.setText("Expanse");
        btnExpanse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExpanseActionPerformed(evt);
            }
        });

        pnlMMM.setBackground(new java.awt.Color(107, 79, 79));

        jPanel1.setBackground(new java.awt.Color(255, 243, 228));

        btnMbar.setBackground(new java.awt.Color(183, 183, 162));
        btnMbar.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        btnMbar.setForeground(new java.awt.Color(72, 52, 52));
        btnMbar.setToolTipText("");
        btnMbar.setPreferredSize(new java.awt.Dimension(81, 45));
        btnMbar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMbarActionPerformed(evt);
            }
        });

        btnMpie.setBackground(new java.awt.Color(183, 183, 162));
        btnMpie.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        btnMpie.setForeground(new java.awt.Color(72, 52, 52));
        btnMpie.setToolTipText("");
        btnMpie.setPreferredSize(new java.awt.Dimension(81, 45));
        btnMpie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMpieActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnMpie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnMbar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(56, 56, 56))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(btnMbar, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMpie, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                .addGap(62, 62, 62))
        );

        Time.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Time.setForeground(new java.awt.Color(72, 52, 52));
        Time.setText("00.00.00 00");

        Date.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Date.setForeground(new java.awt.Color(72, 52, 52));
        Date.setText("00/00/0000");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblhome, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSalename, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnM, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnM1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)
                            .addComponent(pnlSales, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(Date, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(Time, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 178, Short.MAX_VALUE)
                                        .addComponent(btnExpanse, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(14, 14, 14))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(12, 12, 12))))
                            .addComponent(pnlMMM, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblhome, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnExpanse, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Date, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Time, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlMMM, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                    .addComponent(pnlSales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btnM, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(btnM1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtSalename, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnM1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnM1ActionPerformed
        pnlSales.removeAll();
        tblSales.removeAll();
        initPieChart1();
        loadPieDataset1();
        tblSales.setModel(model1);
        txtSalename.setText("November Top 10 Product Sales : ");
        pnlSales.revalidate();
        pnlSales.repaint();
    }//GEN-LAST:event_btnM1ActionPerformed

    private void btnMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMActionPerformed
        pnlSales.removeAll();
        tblSales.removeAll();
        tblSales.setModel(model);
        initPieChart();
        loadPieDataset();
        txtSalename.setText("Top 10 Product Sales All Time : ");
        pnlSales.revalidate();
        pnlSales.repaint();
    }//GEN-LAST:event_btnMActionPerformed

    private void btnExpanseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExpanseActionPerformed
        scrMain.setViewportView(new ExpansePanel(scrMain));
    }//GEN-LAST:event_btnExpanseActionPerformed

    private void btnMpieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMpieActionPerformed

        pnlMMM.removeAll();
        initPieChartM();
        loadPieDatasetM();
        pnlMMM.revalidate();
        pnlMMM.repaint();
    }//GEN-LAST:event_btnMpieActionPerformed

    private void btnMbarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMbarActionPerformed
        pnlMMM.removeAll();
        initBarChart();
        loadBarDataset();
        pnlMMM.revalidate();
        pnlMMM.repaint();
    }//GEN-LAST:event_btnMbarActionPerformed

    void showDate() { //โชว์Date
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date();
        Date.setText(s.format(d));
    }

    void showTime() { //โชว์Time
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                Date d = new Date();
                Time.setText(s.format(d));
            }
        }).start();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnExpanse;
    private javax.swing.JButton btnM;
    private javax.swing.JButton btnM1;
    private javax.swing.JButton btnMbar;
    private javax.swing.JButton btnMpie;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblhome;
    private javax.swing.JPanel pnlMMM;
    private javax.swing.JPanel pnlSales;
    private javax.swing.JTable tblRemains;
    private javax.swing.JTable tblSales;
    private javax.swing.JLabel txtSalename;
    // End of variables declaration//GEN-END:variables
}

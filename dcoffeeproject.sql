--
-- File generated with SQLiteStudio v3.4.3 on Tue Oct 3 18:22:39 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE category (
    category_id   INTEGER PRIMARY KEY,
    category_name TEXT    NOT NULL
);

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         1,
                         'เครื่องดื่ม'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         2,
                         'ขนม'
                     );


-- Table: customer
DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    customer_id    INTEGER   PRIMARY KEY AUTOINCREMENT,
    customer_key   TEXT (50) UNIQUE,
    customer_name  TEXT (50) NOT NULL,
    customer_phone TEXT (50) NOT NULL,
    customer_point INTEGER   DEFAULT (0) 
);

INSERT INTO customer (
                         customer_id,
                         customer_key,
                         customer_name,
                         customer_phone,
                         customer_point
                     )
                     VALUES (
                         1,
                         'CS001',
                         'Sakomtam',
                         '0636478904',
                         0
                     );

INSERT INTO customer (
                         customer_id,
                         customer_key,
                         customer_name,
                         customer_phone,
                         customer_point
                     )
                     VALUES (
                         2,
                         'CS002',
                         'Paramet',
                         '0967652134',
                         10
                     );

INSERT INTO customer (
                         customer_id,
                         customer_key,
                         customer_name,
                         customer_phone,
                         customer_point
                     )
                     VALUES (
                         3,
                         'CS003',
                         'Nawapat',
                         '0625342622',
                         0
                     );

INSERT INTO customer (
                         customer_id,
                         customer_key,
                         customer_name,
                         customer_phone,
                         customer_point
                     )
                     VALUES (
                         4,
                         'CS004',
                         'Paowaric',
                         '0986340987',
                         10
                     );

INSERT INTO customer (
                         customer_id,
                         customer_key,
                         customer_name,
                         customer_phone,
                         customer_point
                     )
                     VALUES (
                         5,
                         'CS005',
                         'Test Customer ',
                         '0999999999',
                         10000
                     );


-- Table: employee
DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
    employee_id       INTEGER   PRIMARY KEY AUTOINCREMENT,
    employee_key      TEXT (50) NOT NULL,
    employee_name     TEXT (50) NOT NULL,
    employee_phone    TEXT (50) NOT NULL,
    employee_email    TEXT (50),
    employee_salary   INTEGER   NOT NULL,
    employee_position TEXT (50) 
);

INSERT INTO employee (
                         employee_id,
                         employee_key,
                         employee_name,
                         employee_phone,
                         employee_email,
                         employee_salary,
                         employee_position
                     )
                     VALUES (
                         1,
                         'E001',
                         'Sakontam Pisuttichaikul',
                         '0815463214',
                         '64160148@go.buu.ac.th',
                         30000,
                         'ผู้จัดการ'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_key,
                         employee_name,
                         employee_phone,
                         employee_email,
                         employee_salary,
                         employee_position
                     )
                     VALUES (
                         2,
                         'E002',
                         'Paramet Tanon',
                         '0967642312',
                         '64160257@go.buu.ac.th',
                         15000,
                         'พนักงาน'
                     );


-- Table: material
DROP TABLE IF EXISTS material;

CREATE TABLE material (
    material_id      INTEGER   PRIMARY KEY AUTOINCREMENT,
    material_key     TEXT (50),
    material_name    TEXT (50) NOT NULL,
    material_price   INTEGER   NOT NULL,
    material_unit    TEXT (50),
    material_minimum INTEGER,
    material_remain  INTEGER
);

INSERT INTO material (
                         material_id,
                         material_key,
                         material_name,
                         material_price,
                         material_unit,
                         material_minimum,
                         material_remain
                     )
                     VALUES (
                         1,
                         'MT001',
                         'AERO Cup 16 oz (50)',
                         99,
                         'pcs',
                         5,
                         9
                     );

INSERT INTO material (
                         material_id,
                         material_key,
                         material_name,
                         material_price,
                         material_unit,
                         material_minimum,
                         material_remain
                     )
                     VALUES (
                         2,
                         'MT002',
                         'Sweetened Condensed Non Dairy Creamer 2 Kg',
                         109,
                         'bag',
                         10,
                         7
                     );

INSERT INTO material (
                         material_id,
                         material_key,
                         material_name,
                         material_price,
                         material_unit,
                         material_minimum,
                         material_remain
                     )
                     VALUES (
                         3,
                         'MT003',
                         'Golden Pearl 1 Kg',
                         45,
                         'bag',
                         10,
                         6
                     );

INSERT INTO material (
                         material_id,
                         material_key,
                         material_name,
                         material_price,
                         material_unit,
                         material_minimum,
                         material_remain
                     )
                     VALUES (
                         4,
                         'MT004',
                         'Brown Sugar 1 kg',
                         42,
                         'unit',
                         5,
                         10
                     );

INSERT INTO material (
                         material_id,
                         material_key,
                         material_name,
                         material_price,
                         material_unit,
                         material_minimum,
                         material_remain
                     )
                     VALUES (
                         5,
                         'MT005',
                         'Non-Dairy Creamer 1000 G',
                         84,
                         'bag',
                         5,
                         5
                     );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id       INTEGER   PRIMARY KEY,
    product_key      TEXT (50) UNIQUE,
    product_name     TEXT (50),
    product_price    INTEGER,
    product_category INTEGER   REFERENCES category (category_id) ON DELETE CASCADE
                                                                 ON UPDATE CASCADE,
    FOREIGN KEY (
        product_category
    )
    REFERENCES category (category_id) ON DELETE CASCADE
                                      ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        1,
                        'PD001',
                        'อเมริกาโน่เย็น',
                        45,
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        2,
                        'PD002',
                        'เค้กชาเขียว',
                        50,
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        3,
                        'PD003',
                        'น้ำเปล่า',
                        60,
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        4,
                        'PD004',
                        'ชานมเย็น',
                        60,
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        5,
                        'PD005',
                        'นมสด',
                        50,
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        6,
                        'PD006',
                        'Brown sugar milk',
                        60,
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        7,
                        'PD007',
                        'มัทฉะ',
                        45,
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        8,
                        'PD008',
                        'ทีรามิสุ ลาเต้',
                        70,
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        9,
                        'PD009',
                        'ชาอู่หลงน้ำผึ้ง',
                        55,
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        10,
                        'PD010',
                        'Chocolate Croffle',
                        70,
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        11,
                        'PD011',
                        'Strawberry Croffle',
                        70,
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_key,
                        product_name,
                        product_price,
                        product_category
                    )
                    VALUES (
                        12,
                        'PD012',
                        'Brownie',
                        30,
                        2
                    );


-- Table: promotion
DROP TABLE IF EXISTS promotion;

CREATE TABLE promotion (
    promotion_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    promotion_name REAL (50) 
);

INSERT INTO promotion (
                          promotion_id,
                          promotion_name
                      )
                      VALUES (
                          1,
                          'โปรวันพฤหัส จัดสักแก้ว'
                      );

INSERT INTO promotion (
                          promotion_id,
                          promotion_name
                      )
                      VALUES (
                          2,
                          'ฟรี 1 แก้ว ต้อนรับฮาโลวีน'
                      );

INSERT INTO promotion (
                          promotion_id,
                          promotion_name
                      )
                      VALUES (
                          3,
                          '100 แต้ม ลด 10 บาท!!'
                      );


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    user_id       INTEGER   PRIMARY KEY AUTOINCREMENT,
    user_login    TEXT (50) NOT NULL,
    user_name     TEXT (50) NOT NULL,
    user_password TEXT (50) NOT NULL,
    user_gender   TEXT (50),
    user_role     INTEGER
);

INSERT INTO user (
                     user_id,
                     user_login,
                     user_name,
                     user_password,
                     user_gender,
                     user_role
                 )
                 VALUES (
                     1,
                     'paramet',
                     'Paramet',
                     'password',
                     'M',
                     1
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_name,
                     user_password,
                     user_gender,
                     user_role
                 )
                 VALUES (
                     2,
                     'user1',
                     'User1',
                     'password',
                     'M',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_name,
                     user_password,
                     user_gender,
                     user_role
                 )
                 VALUES (
                     3,
                     'user2',
                     'User2',
                     'password',
                     'F',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_name,
                     user_password,
                     user_gender,
                     user_role
                 )
                 VALUES (
                     4,
                     'user3',
                     'User 3',
                     'password',
                     'M',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_name,
                     user_password,
                     user_gender,
                     user_role
                 )
                 VALUES (
                     5,
                     'user4',
                     'User 4',
                     'password',
                     'M',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_name,
                     user_password,
                     user_gender,
                     user_role
                 )
                 VALUES (
                     6,
                     'user5',
                     'User 5',
                     'password',
                     'M',
                     2
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
